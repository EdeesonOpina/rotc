@include('layouts.accounts.header')

    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Quotations</h3>
            </div>

<div class="container">
  <div class="text-muted mb-4">&#8592; Back</div>

  <div class="card mb-4">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-borderless">
          <thead>
            <tr>
              <th>Quotation No.</th>
              <th>Category</th>
              <th>Item</th>
              <th>Quantity</th>
              <th>Timer</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <h5>
                  {{ $quotation->full_quotation_num }}
                </h5>
              </td>
              <td>
                <h5>
                  Toiletries
                  {{-- {{ $quotation->category->name }} --}}
                </h5>
              </td>
              <td>
                <h5>
                  Tissue Rolls
                  {{-- {{ $quotation->item->name }} --}}
                </h5>
              </td>
              <td>
                <h5>
                  {{-- 20,000 --}}
                  {{ $quotation->qty }}
                </h5>
              </td>
              <td>
                <h5 class="text-monospace" id="timer">
                  24:00:00
                  {{-- {{ $quotation->start_timer }} --}}
                </h5>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-body">
      <h2>Preferred By Buyer</h2>

      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Buyer</th>
              <th>Year Joined</th>
              <th>Item Sales</th>
              <th>Warranty</th>
              <th>Est. Time (day)</th>
              <th>Cost per piece</th>
              <th>Total Cost</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              {{-- {{}} --}}
              <td>{{ $quotation->user->supplier->name }} </td>
              <td>{{ $quotation->years_joined ?? 'Any' }}</td>
              <td>{{ $quotation->item_sales ?? 'Any' }}</td>
              <td>{{ $quotation->warranty_years 
                ? $quotation->warranty_years . ' years' : 'Any' }}</td>
              <td>{{ $quotation->estimated_time_days 
                ? $quotation->estimated_time_days . ' days' : 'Any' }}</td>
              <td>{{ $quotation->cost_per_piece
                ? 'Php ' . $quotation->cost_per_piece : 'Any' }}</td>
              <td>{{ $quotation->total_cost 
                ? 'Php ' . $quotation->total_cost : 'Any' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Supplier</th>
              <th>SKU (Product)</th>
              <th>Years Joined</th>
              <th>Units Sold</th>
              <th>Est. Time (day)</th>
              <th>Cost per piece</th>
              <th>Total Cost</th>
            </tr>
          </thead>
          <tbody>
            @foreach($quotation->biddings as $bid)
            {{-- {{ dump($bid) }} --}}
            <tr>
              {{-- {{ }} --}}

              {{-- <td>Klennex {{ $bid->product()->name }}</td>  --}}
              <td>{{ $bid->product->supplier->name }}</td> 
              <td>{{ $bid->product->SKU }}</td>
              <td>{{ Carbon\Carbon::now()->year - $bid->product->supplier->created_at->year + 1 . ' years' }}</td>
              <td>{{ $bid->product->units_sold ?? ' - ' }}</td>
              <td>{{ $bid->product->estimated_time_days . ' days' }}</td>
              <td>Php {{ $bid->price }}</td>
              {{-- <td>Php {{ $bid->total }}</td> --}}
              <td>Php {{ $bid->price * $quotation->qty }}</td>
            </tr>
            @endforeach

            {{-- <tr class="table-danger">
              <td>Klennex</td>
              <td>B01FVZH25E</td>
              <td>10 Years</td>
              <td>6,009,290</td>
              <td>10 days</td>
              <td>Php 109.00</td>
              <td>Php 2,180,000</td>
            </tr>
            <tr>
              <td>Klennex</td>
              <td>B01FVZH25E</td>
              <td>10 Years</td>
              <td>6,009,290</td>
              <td>10 days</td>
              <td>Php 109.00</td>
              <td>Php 2,180,000</td>
            </tr>
            <tr class="table-success">
              <td>Klennex</td>
              <td>B01FVZH25E</td>
              <td>10 Years</td>
              <td>6,009,290</td>
              <td>10 days</td>
              <td>Php 109.00</td>
              <td>Php 2,180,000</td>
            </tr>
            <tr>
              <td>Klennex</td>
              <td>B01FVZH25E</td>
              <td>10 Years</td>
              <td>6,009,290</td>
              <td>10 days</td>
              <td>Php 109.00</td>
              <td>Php 2,180,000</td>
            </tr> --}}
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      {{-- <button class="btn btn-outline-dark">Discard</button>
      <button class="btn btn-outline-dark">Edit</button> --}}
      <form method="post" action="{{ url('start-timer/' . $quotation->id) }}">
        {{ csrf_field() }}
        <input type="hidden" name="quotation_id" value="{{ $quotation->id }}">
        {{-- <button type="submit" class="btn btn-primary">Start</button> --}}
      </form>

      <button id="btnStartTimer" class="btn btn-primary">Start</button>
    </div>
  </div>
</div>

@include('layouts.accounts.footer')

<script>
  (function() {
    function Timer(duration, display) {
      let timer = duration,
        hours,
        minutes,
        seconds;
      setInterval(function() {
        hours = parseInt((timer / 3600) % 24, 10);
        minutes = parseInt((timer / 60) % 60, 10);
        seconds = parseInt(timer % 60, 10);

        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.innerHTML = hours + ":" + minutes + ":" + seconds;

        --timer;
      }, 1000);
    }

    const twentyFourHours = 24 * 60 * 60;

    document
      .getElementById("btnStartTimer")
      .addEventListener("click", function() {
        Timer(twentyFourHours - 1, document.getElementById("timer"));
      });
  })();
</script>
