@include('layouts.admins.header')

    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Suppliers</h5>
                      <span class="h2 font-weight-bold mb-0">{{ $suppliers_total_count }}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-store"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Customers</h5>
                      <span class="h2 font-weight-bold mb-0">{{ $customers_total_count }}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Items</h5>
                      <span class="h2 font-weight-bold mb-0">{{ $items_total_count }}</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                        <i class="fas fa-car"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Total Transactions</h5>
                      <span class="h2 font-weight-bold mb-0">0</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-credit-card"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-4">
          <a class="weatherwidget-io" href="https://forecast7.com/en/14d60120d98/manila/" data-label_1="MANILA" data-label_2="WEATHER" data-icons="Climacons Animated" data-font="Roboto" data-theme="orange" >MANILA WEATHER</a>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>
  
    <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Calendar</h6>
                  <h2 class="mb-0">{{ Carbon\Carbon::parse(date('Y-m-d'))->format('l') }}</h2>
                </div>
              </div>
            </div>

            <div class="card-body">
                <h4>{{ Carbon\Carbon::parse(date('Y-m-d'))->format('M d Y') }}</h4>
                <ul>
                  <li>Manage customers</li>
                  <li>Manage your drivers</li>
                </ul>
            </div>

          </div>

        </div>

        <div class="col-xl-8">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>
                  <h2 class="mb-0">Items</h2>
                </div>
              </div>
            </div>
            <div class="card-body">

              <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Supplier</th>
                    <th scope="col">Top Selling Product</th>
                    <th scope="col">Ranking</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($suppliers as $supplier)
                  <tr>
                    <th scope="row">{{ $supplier->id }}</th>
                    <th scope="row">
                      <div class="media align-items-center">
                        @if($supplier->image)
                          <a href="#" class="avatar rounded-circle mr-3">  
                            <img alt="" src="{{ url($supplier->image) }}" style="width: 100%; height: 50px">
                          </a>
                        @else
                          <a href="#" class="avatar mr-3" style="background: #fff"> 
                            <img alt="" src="{{ url('img/icon.png') }}" style="width: 100%; height: 50px">
                          </a>
                        @endif

                        <div class="media-body">
                          <span class="mb-0 text-sm">{{ $supplier->name }}</span>
                        </div>
                      </div>
                    </th>
                    <td>
                      No sales yet
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2"></span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{ rand(0,100) }}%;"></div>
                          </div>
                        </div>
                      </div>
                    </td>

                  </tr>
                  @endforeach
                </tbody>
              </table>
              </div>
            
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-xl-8 mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Top 5 Users</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">

                <thead class="thead-light">
                  <tr>
                    <th scope="col">User</th>
                    <th scope="col">Rating</th>
                    <th scope="col">Last Trip</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        @if($user->avatar)
                          <a href="#" class="avatar rounded-circle mr-3">  
                            <img alt="" src="{{ url($user->avatar) }}" style="width: 100%; height: 50px">
                          </a>
                        @else
                          <a href="#" class="avatar mr-3" style="background: #fff"> 
                            <img alt="" src="{{ url('img/icon.png') }}" style="width: 100%; height: 50px">
                          </a>
                        @endif
                        
                        <div class="media-body">
                          <span class="mb-0 text-sm">{{ $user->firstname }} {{ $user->lastname }}</span>
                        </div>
                      </div>
                    </th>
                    <td>
                      4 stars
                    </td>
                    <td>
                      Metro Manila, Fairview, Quezon City
                    </td>
                  </tr>
                  @endforeach
                </tbody>

              </table>
            </div>
          </div>
        </div>
        <div class="col-xl-4">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Top Suppliers</h3>
                </div>
                <div class="col text-right">
                  <a href="#!" class="btn btn-sm btn-primary">See all</a>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Brand</th>
                    <th scope="col">No. of Products</th>
                    <th scope="col">Performance</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($suppliers as $supplier)
                  <tr>
                    <th scope="row">{{ $supplier->description }}</th>
                    <td>4,569</td>
                    <td>
                      <i class="fas fa-arrow-up text-success mr-3"></i> 46,53%
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
</div>

@include('layouts.admins.footer')