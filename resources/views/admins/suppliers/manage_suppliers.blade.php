@include('layouts.admins.header')

    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Check Out Suppliers</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Supplier</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Address</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($suppliers as $pharmacy)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        @if($pharmacy->image)
                          <a href="#" class="avatar rounded-circle mr-3">  
                            <img alt="" src="{{ url($pharmacy->image) }}" style="width: 100%; height: 50px">
                          </a>
                        @else
                          <a href="#" class="avatar mr-3" style="background: #fff"> 
                            <img alt="" src="{{ url('img/icon.png') }}" style="width: 100%; height: 50px">
                          </a>
                        @endif

                        <div class="media-body">
                          <span class="mb-0 text-sm">{{ $pharmacy->name }}</span>
                        </div>
                      </div>
                    </th>
                    <td>
                      @if($pharmacy->phone_number)
                        {{ $pharmacy->phone_number }} / 
                      @endif

                      {{ $pharmacy->mobile_number }}
                    </td>

                    <td>
                      {{ $pharmacy->address }}
                    </td>

                    <td>

                      @if($pharmacy->status == 0)
                        <span class="badge badge-dot mr-4">
                          <i class="bg-warning"></i> Pending
                        </span>
                      @elseif($pharmacy->status == 1)
                        <span class="badge badge-dot mr-4">
                          <i class="bg-success"></i> Active
                        </span>
                      @elseif($pharmacy->status == '-1')
                        <span class="badge badge-dot mr-4">
                          <i class="bg-danger"></i> Inactive
                        </span>

                      @endif
                    </td>

                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="{{ url('admin/view-pharmacy/'.$pharmacy->id) }}">View</a>
                          <a class="dropdown-item" href="{{ url('admin/edit-pharmacy/'.$pharmacy->id) }}">Edit</a>

                          @if($pharmacy->status == 1)
                            <a class="dropdown-item" href="{{ url('admin/deactivate-pharmacy/'.$pharmacy->id) }}">Deactivate</a>
                          @elseif($pharmacy->status == '-1')
                            <a class="dropdown-item" href="{{ url('admin/activate-pharmacy/'.$pharmacy->id) }}">Activate</a>
                          @elseif($pharmacy->status == 0)
                            <a class="dropdown-item" href="{{ url('admin/activate-pharmacy/'.$pharmacy->id) }}">Activate</a>
                            <a class="dropdown-item" href="{{ url('admin/deactivate-pharmacy/'.$pharmacy->id) }}">Deactivate</a>
                          @endif
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

            <div class="card-footer py-4">
              <nav aria-label="...">
                {{ $suppliers->links() }}
              </nav>
            </div>

          </div>
        </div>
      </div>

      </div>
    </div>
  </div>
</div>

@include('layouts.admins.footer')