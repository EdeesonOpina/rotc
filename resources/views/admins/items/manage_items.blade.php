@include('layouts.admins.header')
<?php
use App\Category;
?>

    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Check Out Items</h3>
            </div>

    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>SKU</th>
            <th></th>
            <th>Item</th>
            <th>Category</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach($items as $item)
          <?php
          $category = Category::find($item->category_id);
          ?>
          <tr>

            <td>{{ str_pad($item->sku, 8, '0', STR_PAD_LEFT) }}</td>
            <td class="py-1"><img src="{{ url($item->image) }}" alt="image" /></td>
            <td>{{ $item->name }}</td>
            <td>{{ $category->description }}</td>
            <td>$ {{ number_format($item->price,2) }}</td>
            <td>{{ $item->qty }}</td>
            <td>
              @if($item->qty > $item->critical_level)
                <label class="badge badge-success">In Stock</label>
              @elseif($item->qty <= 0)
                <label class="badge badge-danger">Out of Stock</label>
              @else
                <label class="badge badge-warning">Almost Empty</label>
              @endif
            </td>
            <td>

              <a href="{{ url('add-item-qty/'.$item->id) }}" style="text-decoration: none;">
                <button class="btn btn-success btn-xs"><i class="mdi mdi-plus"></i>Add Stock</button>
              </a>

              <a href="{{ url('add-item-qty/'.$item->id) }}" style="text-decoration: none;">
                <button class="btn btn-primary btn-xs"><i class="mdi mdi-barcode"></i>Set Barcode</button>
              </a>

            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <br>

    {{ $items->links() }}

  </div>
</div>

@include('layouts.admins.footer')