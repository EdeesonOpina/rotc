@include('layouts.accounts.header')


    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Purchase Order</h3>
            </div>

<div class="container">
  <h2 class="mb-4">Purchase Order Details</h2>
  <form>
    <div class="card mb-4">
      <div class="card-body">
        <div class="form-group row">
          <label for="companyCityStreet" class="col-sm-2 col-form-label"
            >Company City Street</label
          >
          <div class="col-sm-10">
            <input
              type="text"
              class="form-control"
              id="companyCityStreet"
            />
          </div>
        </div>

        <div class="form-group row">
          <label for="companyCity" class="col-sm-2 col-form-label"
            >Company City</label
          >
          <div class="col-sm-10">
            <input type="text" class="form-control" id="companyCity" />
          </div>
        </div>

        <div class="form-group row">
          <label for="companyZip" class="col-sm-2 col-form-label"
            >Company Zip</label
          >
          <div class="col-sm-10">
            <input type="number" class="form-control" id="companyZip" />
          </div>
        </div>

        <div class="form-group row">
          <label for="companyEmail" class="col-sm-2 col-form-label"
            >Company Email</label
          >
          <div class="col-sm-10">
            <input type="email" class="form-control" id="companyEmail" />
          </div>
        </div>

        <div class="form-group row">
          <label for="dateFiled" class="col-sm-2 col-form-label"
            >Date Filed</label
          >
          <div class="col-sm-10">
            <input type="date" class="form-control" id="dateFiled" />
          </div>
        </div>

        <div class="form-group row">
          <label for="fax" class="col-sm-2 col-form-label">Fax</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="fax" />
          </div>
        </div>

        <div class="form-group row">
          <label for="contactName" class="col-sm-2 col-form-label"
            >Contact Name</label
          >
          <div class="col-sm-10">
            <input type="text" class="form-control" id="contactName" />
          </div>
        </div>

        <div class="form-group row">
          <label for="contactEmail" class="col-sm-2 col-form-label"
            >Contact Email</label
          >
          <div class="col-sm-10">
            <input type="email" class="form-control" id="contactEmail" />
          </div>
        </div>
      </div>
    </div>

    <h2 class="mb-4">Assignatories</h2>

    <div class="card mb-4">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Assignatory Name</th>
                <th>Position</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Justo A. Ortiz</td>
                <td>Chairman of the Board of Directors</td>
                <td>Justoostiz@Unionbank.com</td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>Edwin R. Bautista</td>
                <td>Executive Director, President & CEO</td>
                <td>Edwinbaustista@Unionbank.com</td>
                <td>09156748493</td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <input type="text" class="form-control" />
                </td>
                <td>
                  <input type="text" class="form-control" />
                </td>
                <td>
                  <input type="text" class="form-control" />
                </td>
                <td>
                  <input type="text" class="form-control" />
                </td>
                <td>
                  <button
                    class="btn btn-outline-success rounded-circle font-weight-bold"
                  >
                    &#10003;
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="card">
      <div class="card-body">
        <button type="reset" class="btn btn-outline-dark">Discard</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
  </form>
</div>

@include('layouts.accounts.footer')
