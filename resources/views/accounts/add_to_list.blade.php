@include('layouts.accounts.header')
<?php
use App\Category;
use App\Item;
?>

<!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Quotations</h3>
            </div>

	<div class="single_product">
		<div class="container">
			
			<form action="{{ url('add-to-list') }}" method="post">
				
				{{ csrf_field() }}

				<input type="hidden" name="item_id" value="{{ $item->id }}">

				<h3>Please do select list</h3>
				<select name="quotation_id" class="form-control">
					@foreach($quotations as $quotation)
					{{-- <?php
					$item = Item::find($quotation->item_id);
					?> --}}

					<option value="{{ $quotation->id }}"> {{ $quotation->item->name }} 
						- {{ $quotation->full_quotation_num }}</option>

					@endforeach
				</select>

				<br>

				<input type="submit" class="btn btn-primary" value="Select List">

				

			</form>

			<br>

			<h3>or</h3>

			<form action="{{ url('create-new-list') }}" method="post">
				
				{{ csrf_field() }}

				<input type="hidden" name="item_id" value="{{ $item->id }}">

			<input type="submit" class="btn btn-secondary" value="Create New List">

			</form>

			<br><br>

		</div>
	</div>

	</div>
</div>
</div>

@include('layouts.accounts.footer')