@include('layouts.accounts.header')

<!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">

      </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Quotations</h3>
            </div>

	<table class="table table-striped">
	<thead>
		<tr>
			<th>Quotation #</th>
			<th>Category</th>
			<th>Item</th>
			<th>Qty</th>
			<th>Timer</th>
			<th></th>
		</tr>
	</thead>

	<tbody>
		@foreach($quotations as $quotation)
		<?php
		$category = Category::find($quotation->category_id);
		$item = Item::find($quotation->item_id);
		?>

		<tr>
			<td>{{ $quotation->quotation_num }}</td>
			<td>{{ $category->description }}</td>
			<td>{{ $item->name }}</td>
			<td>{{ $quotation->qty }}</td>
			<td>
				@if($quotation->start_timer)
					
				@else
					24:00:00
				@endif
			</td>
			<td>
				<a href="{{ url('biddings/'.$quotation->id) }}" style="text-decoration: none">View</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

</div>
</div>
</div>
</div>

@include('layouts.accounts.footer')