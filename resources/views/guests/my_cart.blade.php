@include('layouts.guests.header')
<?php
use App\Category;
use App\Item;
?>

	
	<div class="cart_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="cart_container">
						<div class="cart_title">My Cart</div>
						<div class="cart_items">
							<ul class="cart_list">
								<li class="cart_item clearfix">


									<div class="cart_item_image">
									@foreach($carts as $cart)
									<?php
									$item = Item::find($cart->item_id);
									?>
										<img src="{{ $item->image }}" alt="" width="100%">
									@endforeach
									</div>
									

									<div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
										<div class="cart_item_name cart_info_col">
											<div class="cart_item_title">Name</div>
											@foreach($carts as $cart)
											<?php
											$item = Item::find($cart->item_id);
											?>

											<div class="cart_item_text">{{ $item->name }}</div>
											@endforeach
										</div>
										<div class="cart_item_color cart_info_col">
											<div class="cart_item_title">Brand</div>
											@foreach($carts as $cart)
											<?php
											$category = Category::find($cart->category_id);
											?>

											<div class="cart_item_text">{{ $category->description }}</div>
											@endforeach
										</div>
										<div class="cart_item_quantity cart_info_col">
											<div class="cart_item_title">Quantity</div>

											@foreach($carts as $cart)
											<div class="cart_item_text">{{ $cart->qty }}</div>
											@endforeach

										</div>
										<div class="cart_item_price cart_info_col">
											<div class="cart_item_title">Price</div>

											@foreach($carts as $cart)
											<?php
											$item = Item::find($cart->item_id);
											?>

											<div class="cart_item_text">₱ {{ number_format($item->price,2) }}</div>
											@endforeach
										</div>
										<div class="cart_item_total cart_info_col">
											<div class="cart_item_title">Total</div>
											@foreach($carts as $cart)
											<div class="cart_item_text">₱ {{ number_format($cart->total,2) }}</div>
											@endforeach
										</div>
									</div>
								</li>
							</ul>
						</div>
						
						<!-- Order Total -->
						<div class="order_total">
							<div class="order_total_content text-md-right">
								<div class="order_total_title">Grand Total:</div>
								<div class="order_total_amount">₱ {{ number_format($carts_total,2) }}</div>
							</div>
						</div>

						<div class="cart_buttons">
							<button type="button" class="button cart_button_checkout">Checkout</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="{{ url('img/logo.png') }}" alt="" width="80px"></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>get our latest promos and offers</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<!-- <div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@include('layouts.guests.footer')