@include('layouts.guests.header')
<?php
use App\Category;
?>

<!-- THE DYNAMIC HEADER -->
<div class="home">
<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ url('guest/images/shop_background.jpg') }}"></div>
<div class="home_overlay"></div>
<div class="home_content d-flex flex-column align-items-center justify-content-center">
	<h2 class="home_title">Shop Materials</h2>
</div>
</div>

	<!-- Shop -->
	<div class="shop">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">

					<!-- Shop Sidebar -->
					<div class="shop_sidebar">
						<div class="sidebar_section">
							<div class="sidebar_title">Categories</div>
							<ul class="sidebar_categories">
								@foreach($categories as $category)
								<li><a href="{{ url('shop/'.$category->id) }}">{{ $category->description }}</a></li>
								@endforeach
							</ul>
						</div>

						<div class="sidebar_section filter_by_section">
							<div class="sidebar_title">Filter By</div>
							<div class="sidebar_subtitle">Price</div>
							<div class="filter_price">
								<div id="slider-range" class="slider_range"></div>
								<p>Range: </p>
								<p><input type="text" id="amount" class="amount" readonly style="border:0; font-weight:bold;"></p>
							</div>
						</div>

						
					</div>

				</div>

				<div class="col-lg-9">
					
					<!-- Shop Content -->

					<div class="shop_content">
						<div class="shop_bar clearfix">
							<div class="shop_product_count"><span>{{ $total_items_count }}</span> products found</div>
							<div class="shop_sorting">
								<span>Sort by:</span>
								<ul>
									<li>
										<span class="sorting_text">highest rated<i class="fas fa-chevron-down"></span></i>
										<ul>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>highest rated</li>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>name</li>
											<li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>price</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>

						<div class="product_grid">
							<div class="product_grid_border"></div>

							@foreach($items as $item)
							<?php
							$category = Category::find($item->category_id);

							$discount = $item->discount / 100;
							$total = $item->price - ($discount * $item->price);
							?>

							<!-- Product Item -->
							<div class="product_item is_new">
								<div class="product_border"></div>
								<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ url($item->image) }}" alt=""></div>
								<div class="product_content">
									<div class="product_price">
										@if($item->discount)
											₱ {{ number_format($total,2) }}<span>₱ {{ number_format($item->price,2) }}</span>
										@else
											₱ {{ number_format($item->price,2) }}
										@endif
									</div>
									<div class="product_name"><div><a href="{{ url('view-item/'.$item->id) }}" tabindex="0">{{ $item->name }}</a></div></div>
								</div>
								<div class="product_fav"><i class="fas fa-heart"></i></div>
								<ul class="product_marks">
									@if($item->discount)
									<li class="product_mark product_new">-{{ $item->discount }}%</li>
									@endif
								</ul>
							</div>
							@endforeach

						</div>

						<!-- Shop Page Navigation -->

						<div class="shop_page_nav d-flex flex-row">
							{{ $items->links() }}
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Recently Viewed -->

	<div class="viewed">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="viewed_title_container">
						<h3 class="viewed_title">Medicines That You Might Like</h3>
						<div class="viewed_nav_container">
							<div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
							<div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
						</div>
					</div>

					<div class="viewed_slider_container">
						
						<!-- Recently Viewed Slider -->

						<div class="owl-carousel owl-theme viewed_slider">
							
							@foreach($featured_items as $featured_item)
							<?php
							$category = Category::find($featured_item->category_id);
							

							$discount = $featured_item->discount / 100;
							$total = $featured_item->price - ($discount * $featured_item->price);
							?>

							<!-- Recently Viewed Item -->
							<div class="owl-item">
								<div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
									<div class="viewed_image"><img src="{{ url($featured_item->image) }}" alt=""></div>
									<div class="viewed_content text-center">
										<div class="viewed_price">
											@if($featured_item->discount)
											₱ {{ number_format($total,2) }}<span>₱ {{ number_format($featured_item->price,2) }}</span>
											@else
												₱ {{ number_format($featured_item->price,2) }}
											@endif
										</div>
										<div class="viewed_name"><a href="{{ url('view-item/'.$featured_item->id) }}">{{ $featured_item->name }}</a></div>
									</div>
									<ul class="item_marks">
										@if($featured_item->discount)
										<li class="item_mark item_discount">-{{ $featured_item->discount }}%</li>
										@endif

										<li class="item_mark item_new">new</li>
									</ul>
								</div>
							</div>
							@endforeach
							
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="{{ url('img/logo.png') }}" alt="" width="80px"></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>get our latest promos and offers</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<!-- <div class="newsletter_unsubscribe_link"><a href="{{ url('#') }}">unsubscribe</a></div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@include('layouts.guests.footer')