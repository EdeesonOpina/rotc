@include('layouts.guests.header')
<?php
use App\Category;
?>

	<!-- Single Product -->

	<div class="single_product">
		<div class="container">
			<div class="row">

				<!-- Images -->
				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						<li data-image="{{ url($item->image) }}"><img src="{{ url($item->image) }}" alt="" width="100%"></li>
						<li data-image="{{ url($item->image) }}"><img src="{{ url($item->image) }}" alt="" width="100%"></li>
						<li data-image="{{ url($item->image) }}"><img src="{{ url($item->image) }}" alt="" width="100%"></li>
					</ul>
				</div>

				<!-- Selected Image -->
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="image_selected"><img src="{{ url($item->image) }}" alt="" width="100%"></div>
				</div>

				<!-- Description -->
				<div class="col-lg-5 order-3">
					<div class="product_description">
						<div class="product_category">{{ $category->description }}</div>
						<div class="product_name">{{ $item->name }}</div>
						<div class="rating_r rating_r_4 product_rating">

                            <span class="fa fa-star" style="color: #f8b600"></span>
		                    <span class="fa fa-star" style="color: #f8b600"></span>
		                    <span class="fa fa-star" style="color: #f8b600"></span>
		                    <span class="fa fa-star" style="color: #f8b600"></span>
		                    <span class="fa fa-star" style="color: #f8b600"></span>

						</div>
						<div class="product_text"><p>{!! $item->description !!}</p></div>
						<div class="order_info d-flex flex-row">
							<form action="{{ url('add-to-cart') }}" method="post">

								{{ csrf_field() }}

								<input type="hidden" name="item_id" value="{{ $item->id }}">

								<div class="clearfix" style="z-index: 1000;">

									<!-- Product Quantity -->
									<div class="product_quantity clearfix">
										<span>Quantity: </span>
										<input id="quantity_input" name="qty" type="text" pattern="[0-9]*" value="1">
										<div class="quantity_buttons">
											<div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
											<div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
										</div>
									</div>

								</div>

								<div class="product_price">
									@if($item->discount)
									<span class="badge badge-success">{{ $item->discount }}% Discount</span><br>
									@endif

									@if($item->discount)
										<span class="badge badge-success">₱ {{ number_format($total,2) }}</span>

										<small>
										<span class="badge badge-danger"><strike>₱ {{ number_format($item->price,2) }}</strike></span>
										</small>
									@else
										₱ {{ number_format($item->price,2) }}
									@endif
								</div>
								<div class="button_container">
									<button type="submit" class="button cart_button">Add to Cart</button>
									<!-- <div class="product_fav"><i class="fas fa-heart"></i></div> -->
								</div>
								
							</form>

							<br>

								

						</div>

						<br>

						<a href="{{ url('add-to-list/'.$item->id) }}" style="text-decoration: none">
								<button class="button cart_button">Add To List</button>
								</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Recently Viewed -->

	<div class="viewed">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="viewed_title_container">
						<h3 class="viewed_title">Supplies That You Might Like</h3>
						<div class="viewed_nav_container">
							<div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
							<div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
						</div>
					</div>

					<div class="viewed_slider_container">
						
						<!-- Recently Viewed Slider -->

						<div class="owl-carousel owl-theme viewed_slider">
							
							@foreach($featured_items as $featured_item)
							<?php
							$category = Category::find($featured_item->category_id);

							$discount = $featured_item->discount / 100;
							$total = $featured_item->price - ($discount * $featured_item->price);
							?>

							<!-- Recently Viewed Item -->
							<div class="owl-item">
								<div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
									<div class="viewed_image"><img src="{{ url($featured_item->image) }}" alt=""></div>
									<div class="viewed_content text-center">
										<div class="viewed_price">
											@if($featured_item->discount)
											₱ {{ number_format($total,2) }}<span>₱ {{ number_format($featured_item->price,2) }}</span>
											@else
												₱ {{ number_format($featured_item->price,2) }}
											@endif
										</div>
										<div class="viewed_name"><a href="#">{{ $featured_item->name }}</a></div>
									</div>
									<ul class="item_marks">
										@if($featured_item->discount)
										<li class="item_mark item_discount">-{{ $featured_item->discount }}%</li>
										@endif

										<li class="item_mark item_new">new</li>
									</ul>
								</div>
							</div>
							@endforeach
							
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="{{ url('img/logo.png') }}" alt="" width="80px"></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>get our latest promos and offers</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<!-- <div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@include('layouts.guests.footer')