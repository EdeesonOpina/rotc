@include('layouts.guests.header')
<?php
use App\Category;
use App\Brand;
?>

<div class="shop">
  <div class="container">

    <div class="row">

      <div class="col-lg-3">

        <img src="{{ url('guest/images/icon.png') }}" width="100%">

      </div>

      <div class="col-lg-1">
        &nbsp;
      </div>

      <div class="col-lg-8">
        
        <h2>Register</h2>

        <div class="row">
            <div class="col-md-8">
              @if($errors->any())
              <div class="alert alert-danger" role="alert">
                <h5 class="alert-heading">Oops! We have an error here.</h5>
                <br>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </div>
              @endif

              @if(Session::get('error'))
              <div class="alert alert-danger" role="alert">
                <h5 class="alert-heading">Oops! We have an error here.</h5>
                <br>
                {{ Session::get('error') }}
              </div>
              @endif
            </div>
        </div>

        <form role="form" action="{{ url('register') }}" method="post">

          {{ csrf_field() }}

          <div class="row">

            <div class="col-md-8">
              <input class="form-control" name="firstname" placeholder="Firstname" type="text">
            </div>

          </div> 

          <br>

          <div class="row">

            <div class="col-md-8">
              <input class="form-control" name="lastname" placeholder="Lastname" type="text">
            </div>

          </div>

          <br>

          <div class="row">

            <div class="col-md-8">
              <input class="form-control" name="email" placeholder="Email" type="email">
            </div>

          </div>

          <br>

          <div class="row">

            <div class="col-md-8">
              <input class="form-control" name="password" placeholder="Password" type="password">
            </div>

          </div>

          <br>

          <div class="row">

            <div class="col-md-8">
              <input class="form-control" name="confirm_password" placeholder="Confirm Password" type="password">
            </div>

          </div> 

          <br>

          <button type="submit" class="btn btn-primary" style="background: #09DAD0; border: 1px solid #09DAD0">Sign in</button>

        </form>
        
        <br>

        Forgot password? <a href="{{ url('forgot-password') }}" style="color: #09DAD0;">Click here</a>

        <br>

        Already have an account? <a href="{{ url('login') }}" style="color: #09DAD0;">Click here</a>

      </div>

    </div>
  </div>
</div>


@include('layouts.guests.footer')