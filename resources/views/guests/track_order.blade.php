@include('layouts.guests.header')
<?php
use App\Category;
use App\Brand;
?>


<div class="shop">
	<div class="container">
		<div class="row">

			<div class="col-lg-3">

				<img src="{{ url('guest/images/icon.png') }}" width="100%">

			</div>

			<div class="col-lg-1">
				&nbsp;
			</div>

			<div class="col-lg-8">
				
				<h2>Track Order</h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<br>

				<b>Enter your transaction number here</b>

				<br><br>

				<form action="{{ url('track-order') }}" method="post">
					
					{{ csrf_field() }}

					<div class="row">

						<div class="col-md-6">
							<input type="text" class="form-control" name="transaction_number" placeholder="Enter transaction number here...">
						</div>

						<div class="col-md-4">
							<input type="submit" class="btn btn-primary" style="background: #09DAD0; border: 1px solid #09DAD0" value="Search">
						</div>

					</div>

				</form>
				

			</div>

		</div>
	</div>
</div>


@include('layouts.guests.footer')