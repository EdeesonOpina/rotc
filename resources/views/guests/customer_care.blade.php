@include('layouts.guests.header')
<?php
use App\Category;
use App\Brand;
?>


<div class="shop">
	<div class="container">
		<div class="row">

			<div class="col-lg-3">

				<img src="{{ url('guest/images/icon.png') }}" width="100%">

			</div>

			<div class="col-lg-1">
				&nbsp;
			</div>

			<div class="col-lg-8">
				
				<h2>Customer Care</h2>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<br>

				<h4><span class="fa fa-envelope" style="margin-right: 8px"></span>hello@pillmarket.ph</h4>
				<br>
				<h4><span class="fa fa-phone" style="margin-right: 8px"></span>(02) 111 22 33</h4>
				<br>
				<h4><span class="fa fa-mobile-alt" style="margin-right: 16px"></span>+639223334444</h4>
				

			</div>

		</div>
	</div>
</div>


@include('layouts.guests.footer')