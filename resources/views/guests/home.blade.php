<?php
use App\Category;
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Aluna PH | UHACK 2019</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Aluna PH | UHACK 2019">

<!-- Favicon -->
<link href="{{ url('img/icon.png') }}" rel="icon" type="image/png">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="guest/styles/bootstrap4/bootstrap.min.css">
<link href="guest/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="guest/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="guest/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="guest/plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="guest/plugins/slick-1.8.0/slick.css">
<link rel="stylesheet" type="text/css" href="guest/styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="guest/styles/responsive.css">

</head>

<body>

<div class="super_container">
	
	<!-- Header -->
	
	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar" style="background: #FD5F32;">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<!-- <div class="top_bar_contact_item"><div class="top_bar_icon"><img src="guest/images/phone.png" alt=""></div>+38 068 005 3570</div> -->
						<div class="top_bar_contact_item"><div class="top_bar_icon"><img src="guest/images/mail.png" alt=""></div><a href="mailto:support@aluna.ph">support@aluna.ph</a></div>
						<div class="top_bar_content ml-auto">
							<div class="top_bar_menu">

								<!-- <ul class="standard_dropdown top_bar_dropdown">

									<li>
										<a href="#">$ US dollar<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li><a href="#">EUR Euro</a></li>
											<li><a href="#">GBP British Pound</a></li>
											<li><a href="#">JPY Japanese Yen</a></li>
										</ul>
									</li>

								</ul> -->

							</div>
							<div class="top_bar_user">
								<div class="user_icon"><img src="guest/images/user.svg" alt=""></div>
								<div><a href="{{ url('register') }}">Register</a></div>
								<div><a href="{{ url('login') }}">Sign in</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<!-- Header Main -->

		<div class="header_main">
			<div class="container">
				<div class="row">

					<!-- Logo -->
					<div class="col-lg-2 col-sm-3 col-3 order-1">
						<div class="logo_container">
							<div class="logo"><a href="{{ url('/') }}"><img src="{{ url('guest/images/logo.png') }}" width="150px"></a></div>
						</div>
					</div>

					<!-- Search -->
					<div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
						<div class="header_search">
							<div class="header_search_content">
								<div class="header_search_form_container">
									<form action="#" class="header_search_form clearfix">
										<input type="search" required="required" class="header_search_input" placeholder="Search for products...">
										<div class="custom_dropdown">
											<div class="custom_dropdown_list">
												<span class="custom_dropdown_placeholder clc">All Categories</span>
												<i class="fas fa-chevron-down"></i>
												<ul class="custom_list clc">
													<li><a class="clc" href="{{ url('shop/*') }}">All Categories</a></li>
													@foreach($categories as $category)
													<li><a class="clc" href="{{ url('shop/'.$category->id) }}">{{ $category->description }}</a></li>
													@endforeach
												</ul>
											</div>
										</div>
										<button type="submit" class="header_search_button trans_300" value="Submit"><img src="guest/images/search.png" alt=""></button>
									</form>
								</div>
							</div>
						</div>
					</div>

					<!-- Wishlist -->
					<div class="col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right">
						<div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
							<div class="wishlist d-flex flex-row align-items-center justify-content-end">
								<div class="wishlist_icon"><img src="guest/images/heart.png" alt=""></div>
								<div class="wishlist_content">
									<div class="wishlist_text"><a href="#">Wishlist</a></div>
									<div class="wishlist_count">0</div>
								</div>
							</div>

							<!-- Cart -->
							<div class="cart">
								<div class="cart_container d-flex flex-row align-items-center justify-content-end">
									<div class="cart_icon">
										<a href="{{ url('my-cart') }}">
										<img src="guest/images/cart.png" alt="">
										<div class="cart_count"><span>0</span></div>
										</a>
									</div>
									<div class="cart_content">
										<div class="cart_text"><a href="{{ url('my-cart') }}">Cart</a></div>
										<div class="cart_price">₱ {{ number_format(85,2) }}</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col">
						
						<div class="main_nav_content d-flex flex-row">

							<!-- Categories Menu -->

							<div class="cat_menu_container">
								<div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
									<div class="cat_burger"><span></span><span></span><span></span></div>
									<div class="cat_menu_text">categories</div>
								</div>

								<ul class="cat_menu">
									@foreach($categories as $category)
									<li><a href="#">{{ $category->description }}<i class="fas fa-chevron-right"></i></a></li>
									@endforeach
									<!-- <li class="hassubs">
										<a href="#">Hardware<i class="fas fa-chevron-right"></i></a>
										<ul>
											<li class="hassubs">
												<a href="#">Menu Item<i class="fas fa-chevron-right"></i></a>
												<ul>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
												</ul>
											</li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
										</ul>
									</li> -->

								</ul>
							</div>

							<!-- Main Nav Menu -->

							<div class="main_nav_menu ml-auto">
								<ul class="standard_dropdown main_nav_dropdown">
									<li><a href="{{ url('shop') }}" style="color: #000 !important;">Shop<i class="fas fa-chevron-down"></i></a></li>
									<li class="hassubs">
										<a href="#" style="color: #000 !important;">Top Deals<i class="fas fa-chevron-down"></i></a>
										<ul>
											@foreach($categories as $category)
											<li><a href="{{ url('shop/'.$category->id) }}">{{ $category->description }}<i class="fas fa-chevron-down"></i></a></li>
											@endforeach
										</ul>
									</li>



									<li><a href="{{ url('/') }}" style="color: #000 !important;">Customer Care<i class="fas fa-chevron-down"></i></a></li>

									<li><a href="{{ url('/') }}" style="color: #000 !important;">Track My Order<i class="fas fa-chevron-down"></i></a></li>
									
								</ul>
							</div>

							<!-- Menu Trigger -->

							<div class="menu_trigger_container ml-auto">
								<div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
									<div class="menu_burger">
										<div class="menu_trigger_text">menu</div>
										<div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</nav>
		
		<!-- Menu -->

		<div class="page_menu">
			<div class="container">
				<div class="row">
					<div class="col">
						
						<div class="page_menu_content">
							
							<div class="page_menu_search">
								<form action="#">
									<input type="search" required="required" class="page_menu_search_input" placeholder="Search for products...">
								</form>
							</div>
							<ul class="page_menu_nav">
								<li class="page_menu_item has-children">
									<a href="#">Language<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Italian<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Spanish<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Japanese<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item has-children">
									<a href="#">Currency<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">US Dollar<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">EUR Euro<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">GBP British Pound<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">JPY Japanese Yen<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item">
									<a href="#">Home<i class="fa fa-angle-down"></i></a>
								</li>
								<li class="page_menu_item has-children">
									<a href="#">Super Deals<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">Super Deals<i class="fa fa-angle-down"></i></a></li>
										<li class="page_menu_item has-children">
											<a href="#">Menu Item<i class="fa fa-angle-down"></i></a>
											<ul class="page_menu_selection">
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
												<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
											</ul>
										</li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>

								<li class="page_menu_item has-children">
									<a href="#">Trending Styles<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">Trending Styles<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item"><a href="blog.html">blog<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a href="contact.html">contact<i class="fa fa-angle-down"></i></a></li>
							</ul>
							
							<div class="menu_contact">
								<div class="menu_contact_item"><div class="menu_contact_icon"><img src="guest/images/phone_white.png" alt=""></div>+38 068 005 3570</div>
								<div class="menu_contact_item"><div class="menu_contact_icon"><img src="guest/images/mail_white.png" alt=""></div><a href="mailto:fastsales@gmail.com">fastsales@gmail.com</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</header>
	
	<!-- Banner -->

	<div class="banner">
		<div class="banner_background" style="background-image:url(img/banner_background.jpg)"></div>
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="banner_product_image"><img src="img/logo.png" alt="" width="400px"></div>
				<div class="col-lg-5 offset-lg-4 fill_height">
					<div class="banner_content">
						<h1 class="banner_text" style="color: #333;">UHACK 2019</h1>
						<h4>Real Estate and Livable Cities Hackathon</h4>
						<div class="button banner_button"><a href="#">Shop Now</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Characteristics -->

	<div class="characteristics">
		<div class="container">
			<div class="row">

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><span class="fa fa-truck" style="font-size: 30px; color: #FD5F32"></span></div>
						<div class="char_content">
							<div class="char_title">Express Delivery</div>
							<div class="char_subtitle">from ₱ 50.00</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><span class="fa fa-ticket" style="font-size: 30px; color: #FD5F32"></span></div>
						<div class="char_content">
							<div class="char_title">Quotations</div>
							<div class="char_subtitle">Search the best deals</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><span class="fa fa-credit-card" style="font-size: 30px; color: #FD5F32"></span></div>
						<div class="char_content">
							<div class="char_title">Easy Payment</div>
							<div class="char_subtitle">from ₱ 50.00</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-3 col-md-6 char_col">
					
					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><span class="fa fa-handshake" style="font-size: 30px; color: #FD5F32"></span></div>
						<div class="char_content">
							<div class="char_title">Cash on Delivery</div>
							<div class="char_subtitle">from ₱ 50.00</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Deals of the week -->

	<div class="deals_featured">
		<div class="container">
			<div class="row">
				<div class="col d-flex flex-lg-row flex-column align-items-center justify-content-start">
					
					<!-- Deals -->

					<div class="deals">
						<div class="deals_title">Deals of the Week</div>
						<div class="deals_slider_container">
							
							<!-- Deals Slider -->
							<div class="owl-carousel owl-theme deals_slider">
								
								@foreach($deals_items as $deals_item)
								<?php
								$category = Category::find($deals_item->category_id);
								

								$discount = $deals_item->discount / 100;
								$total = $deals_item->price - ($discount * $deals_item->price);
								?>

								<!-- Deals Item -->
								<div class="owl-item deals_item">
									<div class="deals_image"><img src="{{ $deals_item->image }}" alt=""></div>
									<div class="deals_content">
										<div class="deals_info_line d-flex flex-row justify-content-start">
											
											<div class="deals_item_price_a ml-auto">₱ {{ number_format($deals_item->price,2) }}</div>
										</div>
										<div class="deals_info_line d-flex flex-row justify-content-start">
											<div class="deals_item_name">{{ $deals_item->name }}</div>
											<div class="deals_item_price ml-auto">{{ number_format($total,2) }}</div>
										</div>
										<div class="available">
											<div class="available_line d-flex flex-row justify-content-start">
												<div class="available_title">Available: <span>6</span></div>
												<div class="sold_title ml-auto">Already sold: <span>28</span></div>
											</div>
											<div class="available_bar"><span style="width:17%"></span></div>
										</div>
										<div class="deals_timer d-flex flex-row align-items-center justify-content-start">
											<div class="deals_timer_title_container">
												<div class="deals_timer_title">Hurry Up</div>
												<div class="deals_timer_subtitle">Offer ends in:</div>
											</div>
											<div class="deals_timer_content ml-auto">
												<div class="deals_timer_box clearfix" data-target-time="">
													<div class="deals_timer_unit">
														<div id="deals_timer1_hr" class="deals_timer_hr"></div>
														<span>hours</span>
													</div>
													<div class="deals_timer_unit">
														<div id="deals_timer1_min" class="deals_timer_min"></div>
														<span>mins</span>
													</div>
													<div class="deals_timer_unit">
														<div id="deals_timer1_sec" class="deals_timer_sec"></div>
														<span>secs</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endforeach

							</div>

						</div>

						<div class="deals_slider_nav_container">
							<div class="deals_slider_prev deals_slider_nav"><i class="fas fa-chevron-left ml-auto"></i></div>
							<div class="deals_slider_next deals_slider_nav"><i class="fas fa-chevron-right ml-auto"></i></div>
						</div>
					</div>
					
					<!-- Featured -->
					<div class="featured">
						<div class="tabbed_container">
							<div class="tabs">
								<ul class="clearfix">
									<li class="active">Featured</li>
									<li>On Sale</li>
									<li>Best Rated</li>
								</ul>
								<div class="tabs_line"><span></span></div>
							</div>

							<!-- Product Panel -->
							<div class="product_panel panel active">
								<div class="featured_slider slider">

									@foreach($featured_items as $featured_item)
									<?php
									$category = Category::find($featured_item->category_id);
									

									$discount = $featured_item->discount / 100;
									$total = $featured_item->price - ($discount * $featured_item->price);
									?>

									<!-- Slider Item -->
									<div class="featured_slider_item">
										<div class="border_active"></div>
										<div class="product_item discount d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ url($featured_item->image) }}" alt="" width="80%"></div>
											<div class="product_content">
												<div class="product_price discount">₱ {{ number_format($total,2) }}<span>₱ {{ number_format($featured_item->price,2) }}</span></div>
												<div class="product_name"><div><a href="{{ url('view-item/'.$featured_item->id) }}">{{ $featured_item->name }}</a></div></div>
											</div>
											<div class="product_fav"><i class="fas fa-heart"></i></div>
											<div class="product_extras">
												
												<button class="product_cart_button">Add to Cart</button>
											</div>
											<ul class="product_marks">

												@if($featured_item->discount)
												<li class="product_mark product_discount">-{{ $featured_item->discount }}%</li>
												@endif
												<li class="product_mark product_new">new</li>
											</ul>
										</div>
									</div>
									@endforeach

								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>

							<!-- Product Panel -->

							<div class="product_panel panel">
								<div class="featured_slider slider">

									@foreach($sale_items as $sale_item)
									<?php
									$category = Category::find($sale_item->category_id);
									

									$discount = $sale_item->discount / 100;
									$total = $sale_item->price - ($discount * $sale_item->price);
									?>

									<!-- Slider Item -->
									<div class="featured_slider_item">
										<div class="border_active"></div>
										<div class="product_item discount d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ url($sale_item->image) }}" alt="" width="80%"></div>
											<div class="product_content">
												<div class="product_price discount">₱ {{ number_format($total,2) }}<span>₱ {{ number_format($sale_item->price,2) }}</span></div>
												<div class="product_name"><div><a href="{{ url('view-item/'.$sale_item->id) }}">{{ $sale_item->name }}</a></div></div>
											</div>
											<div class="product_fav"><i class="fas fa-heart"></i></div>
											<div class="product_extras">
												
												<button class="product_cart_button">Add to Cart</button>
											</div>
											<ul class="product_marks">
												@if($sale_item->discount)
												<li class="product_mark product_discount">-{{ $sale_item->discount }}%</li>
												@endif
												<li class="product_mark product_new">new</li>
											</ul>
										</div>
									</div>
									@endforeach

								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>

							<!-- Product Panel -->

							<div class="product_panel panel">
								<div class="featured_slider slider">

									@foreach($hottest_items as $hottest_item)
									<?php
									$category = Category::find($hottest_item->category_id);
									

									$discount = $hottest_item->discount / 100;
									$total = $hottest_item->price - ($discount * $hottest_item->price);
									?>

									<!-- Slider Item -->
									<div class="featured_slider_item">
										<div class="border_active"></div>
										<div class="product_item discount d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ url($hottest_item->image) }}" alt="" width="80%"></div>
											<div class="product_content">
												<div class="product_price discount">₱ {{ number_format($total,2) }}<span>₱ {{ number_format($hottest_item->price,2) }}</span></div>
												<div class="product_name"><div><a href="{{ url('view-item/'.$hottest_item->id) }}">{{ $hottest_item->name }}</a></div></div>
											</div>
											<div class="product_fav"><i class="fas fa-heart"></i></div>
											<div class="product_extras">
												
												<button class="product_cart_button">Add to Cart</button>
											</div>
											<ul class="product_marks">
												@if($hottest_item->discount)
												<li class="product_mark product_discount">-{{ $hottest_item->discount }}%</li>
												@endif
												<li class="product_mark product_new">new</li>
											</ul>
										</div>
									</div>
									@endforeach

								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	
	<!-- Hot New Arrivals -->

	<div class="new_arrivals">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="tabbed_container">
						<div class="tabs clearfix tabs-right">
							<div class="new_arrivals_title">Hot New Arrivals</div>
							<ul class="clearfix">
								<li class="active">Featured</li>
								<li>Top 10</li>
								<li>Cheap Offers</li>
							</ul>
							<div class="tabs_line"><span></span></div>
						</div>
						<div class="row">
							<div class="col-lg-9" style="z-index:1;">

								<!-- Product Panel -->
								<div class="product_panel panel active">
									<div class="arrivals_slider slider">

										@foreach($newest_items as $newest_item)
										

										<!-- Slider Item -->
										<div class="arrivals_slider_item">
											<div class="border_active"></div>
											<div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
												<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ $newest_item->image }}" alt="" width="80%"></div>
												<div class="product_content">
													<div class="product_price">₱ {{ number_format($newest_item->price,2) }}</div>
													<div class="product_name"><div><a href="{{ url('view-item/'.$newest_item->id) }}">{{ $newest_item->name }}</a></div></div>
													<div class="product_extras">
														
														<button class="product_cart_button">Add to Cart</button>
													</div>
												</div>
												<div class="product_fav"><i class="fas fa-heart"></i></div>
												<ul class="product_marks">
													<li class="product_mark product_discount">-25%</li>
													<li class="product_mark product_new">new</li>
												</ul>
											</div>
										</div>
										@endforeach

									</div>
									<div class="arrivals_slider_dots_cover"></div>
								</div>

								<!-- Product Panel -->
								<div class="product_panel panel">
									<div class="arrivals_slider slider">

										@foreach($top_10_newest_items as $top_10_newest_item)
										

										<!-- Slider Item -->
										<div class="arrivals_slider_item">
											<div class="border_active"></div>
											<div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
												<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ $top_10_newest_item->image }}" alt="" width="80%"></div>
												<div class="product_content">
													<div class="product_price">₱ {{ number_format($top_10_newest_item->price,2) }}</div>
													<div class="product_name"><div><a href="{{ url('view-item/'.$top_10_newest_item->id) }}">{{ $top_10_newest_item->name }}</a></div></div>
													<div class="product_extras">
														
														<button class="product_cart_button">Add to Cart</button>
													</div>
												</div>
												<div class="product_fav"><i class="fas fa-heart"></i></div>
												<ul class="product_marks">
													<li class="product_mark product_discount">-25%</li>
													<li class="product_mark product_new">new</li>
												</ul>
											</div>
										</div>
										@endforeach

									</div>
									<div class="arrivals_slider_dots_cover"></div>
								</div>

								<!-- Product Panel -->
								<div class="product_panel panel">
									<div class="arrivals_slider slider">

										@foreach($cheap_newest_items as $cheap_newest_item)
										

										<!-- Slider Item -->
										<div class="arrivals_slider_item">
											<div class="border_active"></div>
											<div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
												<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ $cheap_newest_item->image }}" alt="" width="80%"></div>
												<div class="product_content">
													<div class="product_price">₱ {{ number_format($cheap_newest_item->price,2) }}</div>
													<div class="product_name"><div><a href="{{ url('view-item/'.$cheap_newest_item->id) }}">{{ $cheap_newest_item->name }}</a></div></div>
													<div class="product_extras">
														
														<button class="product_cart_button">Add to Cart</button>
													</div>
												</div>
												<div class="product_fav"><i class="fas fa-heart"></i></div>
												<ul class="product_marks">
													<li class="product_mark product_discount">-25%</li>
													<li class="product_mark product_new">new</li>
												</ul>
											</div>
										</div>
										@endforeach

									</div>
									<div class="arrivals_slider_dots_cover"></div>
								</div>

							</div>

							<div class="col-lg-3">
								@foreach($top_newest_items as $top_newest_item)
								<?php
								$category = Category::find($deals_item->category_id);
								
								?>

								<div class="arrivals_single clearfix">
									<div class="d-flex flex-column align-items-center justify-content-center">
										<div class="arrivals_single_image"><img src="{{ $top_newest_item->image }}" alt=""></div>
										<div class="arrivals_single_content">
											<div class="arrivals_single_category"><a href="#">{{ $category->description }}</a></div>
											<div class="arrivals_single_name_container clearfix">
												<div class="arrivals_single_name"><a href="#">{{ $top_newest_item->name }}</a></div>
												<div class="arrivals_single_price text-right">₱ {{ number_format($top_newest_item->price,2) }}</div>
											</div>
											
											<form action="#"><button class="arrivals_single_button">Add to Cart</button></form>
										</div>
										<div class="arrivals_single_fav product_fav active"><i class="fas fa-heart"></i></div>
										<ul class="arrivals_single_marks product_marks">
											<li class="arrivals_single_mark product_mark product_new">new</li>
										</ul>
									</div>
								</div>
								@endforeach

							</div>

						</div>
								
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Best Sellers -->

	<div class="best_sellers">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="tabbed_container">
						<div class="tabs clearfix tabs-right">
							<div class="new_arrivals_title">Hot Best Sellers</div>
							<ul class="clearfix">
								<li class="active">Top 20</li>
								<li>Best Selling</li>
								<li>Cheap Offers</li>
							</ul>
							<div class="tabs_line"><span></span></div>
						</div>

						<div class="bestsellers_panel panel active">

							<!-- Best Sellers Slider -->

							<div class="bestsellers_slider slider">

								@foreach($top_hottest_items as $top_hottest_item)
								<?php
								$category = Category::find($top_hottest_item->category_id);
								

								$discount = $top_hottest_item->discount / 100;
								$total = $top_hottest_item->price - ($discount * $top_hottest_item->price);
								?>

								<!-- Best Sellers Item -->
								<div class="bestsellers_item discount">
									<div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
										<div class="bestsellers_image"><img src="{{ $top_hottest_item->image }}" alt=""></div>
										<div class="bestsellers_content">
											<div class="bestsellers_category"><a href="#">{{ $category->description }}</a></div>
											<div class="bestsellers_name"><a href="{{ url('view-item/'.$top_hottest_item->id) }}">{{ $top_hottest_item->name }}</a></div>
											
											<div class="bestsellers_price discount">₱	 {{ number_format($total,2) }}<span>₱	 {{ number_format($top_hottest_item->price,2) }}</span></div>
										</div>
									</div>
									<div class="bestsellers_fav active"><i class="fas fa-heart"></i></div>
									<ul class="bestsellers_marks">
										@if($top_hottest_item->discount)
										<li class="bestsellers_mark bestsellers_discount">-{{ $top_hottest_item->discount }}%</li>
										@endif
										<li class="bestsellers_mark bestsellers_new">new</li>
									</ul>
								</div>
								@endforeach

							</div>
						</div>

						<div class="bestsellers_panel panel">

							<!-- Best Sellers Slider -->

							<div class="bestsellers_slider slider">

								@foreach($top_10_hottest_items as $top_10_hottest_item)
								<?php
								$category = Category::find($top_10_hottest_item->category_id);
								

								$discount = $top_10_hottest_item->discount / 100;
								$total = $top_10_hottest_item->price - ($discount * $top_10_hottest_item->price);
								?>

								<!-- Best Sellers Item -->
								<div class="bestsellers_item discount">
									<div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
										<div class="bestsellers_image"><img src="{{ $top_10_hottest_item->image }}" alt=""></div>
										<div class="bestsellers_content">
											<div class="bestsellers_category"><a href="#">{{ $category->description }}</a></div>
											<div class="bestsellers_name"><a href="{{ url('view-item/'.$top_10_hottest_item->id) }}">{{ $top_10_hottest_item->name }}</a></div>
											
											<div class="bestsellers_price discount">₱	 {{ number_format($total,2) }}<span>₱	 {{ number_format($top_10_hottest_item->price,2) }}</span></div>
										</div>
									</div>
									<div class="bestsellers_fav active"><i class="fas fa-heart"></i></div>
									<ul class="bestsellers_marks">
										@if($top_10_hottest_item->discount)
										<li class="bestsellers_mark bestsellers_discount">-{{ $top_10_hottest_item->discount }}%</li>
										@endif
										<li class="bestsellers_mark bestsellers_new">new</li>
									</ul>
								</div>
								@endforeach

							</div>
						</div>

						<div class="bestsellers_panel panel">

							<!-- Best Sellers Slider -->

							<div class="bestsellers_slider slider">

								@foreach($cheap_hottest_items as $cheap_hottest_item)
								<?php
								$category = Category::find($cheap_hottest_item->category_id);
								

								$discount = $cheap_hottest_item->discount / 100;
								$total = $cheap_hottest_item->price - ($discount * $cheap_hottest_item->price);
								?>

								<!-- Best Sellers Item -->
								<div class="bestsellers_item discount">
									<div class="bestsellers_item_container d-flex flex-row align-items-center justify-content-start">
										<div class="bestsellers_image"><img src="{{ $cheap_hottest_item->image }}" alt=""></div>
										<div class="bestsellers_content">
											<div class="bestsellers_category"><a href="#">{{ $category->description }}</a></div>
											<div class="bestsellers_name"><a href="{{ url('view-item/'.$cheap_hottest_item->id) }}">{{ $cheap_hottest_item->name }}</a></div>
											
											<div class="bestsellers_price discount">₱	 {{ number_format($total,2) }}<span>₱	 {{ number_format($cheap_hottest_item->price,2) }}</span></div>
										</div>
									</div>
									<div class="bestsellers_fav active"><i class="fas fa-heart"></i></div>
									<ul class="bestsellers_marks">
										@if($cheap_hottest_item->discount)
										<li class="bestsellers_mark bestsellers_discount">-{{ $cheap_hottest_item->discount }}%</li>
										@endif
										<li class="bestsellers_mark bestsellers_new">new</li>
									</ul>
								</div>
								@endforeach

							</div>
						</div>
					</div>
						
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="{{ url('img/logo.png') }}" alt="" width="80px"></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>get our latest promos and offers</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<!-- <div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="{{ url('/') }}"><img src="{{ url('img/logo.png') }}" width="130px"></a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone">+63 966 777 8888</div>
						<div class="footer_contact_text">
							<p>17 Princess Road, London</p>
							<p>Grester London NW18JR, UK</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-2">
					<div class="footer_column">
						<div class="footer_title">Find your medicine</div>
						<ul class="footer_list">
							@foreach($categories as $category)
							<li><a href="{{ url('shop/'.$category->id) }}">{{ $category->description }}</a></li>
							@endforeach
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<div class="footer_title">Customer Care</div>
						<ul class="footer_list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order Tracking</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Customer Services</a></li>
							<li><a href="#">Returns / Exchange</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Product Support</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>

	<!-- Copyright -->

	<div class="copyright" style="background: #FD5F32;">
		<div class="container">
			<div class="row">
				<div class="col">
					
					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start" >
						<div class="copyright_content" style="color: #fff;"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Aluna
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
						<!-- <div class="logos ml-sm-auto">
							<ul class="logos_list">
								<li><a href="#"><img src="guest/images/logos_1.png" alt=""></a></li>
								<li><a href="#"><img src="guest/images/logos_2.png" alt=""></a></li>
								<li><a href="#"><img src="guest/images/logos_3.png" alt=""></a></li>
								<li><a href="#"><img src="guest/images/logos_4.png" alt=""></a></li>
							</ul>
						</div> -->

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="guest/js/jquery-3.3.1.min.js"></script>
<script src="guest/styles/bootstrap4/popper.js"></script>
<script src="guest/styles/bootstrap4/bootstrap.min.js"></script>
<script src="guest/plugins/greensock/TweenMax.min.js"></script>
<script src="guest/plugins/greensock/TimelineMax.min.js"></script>
<script src="guest/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="guest/plugins/greensock/animation.gsap.min.js"></script>
<script src="guest/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="guest/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="guest/plugins/slick-1.8.0/slick.js"></script>
<script src="guest/plugins/easing/easing.js"></script>
<script src="guest/js/custom.js"></script>
</body>

</html>