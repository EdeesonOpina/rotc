
<div style="margin-top: 50px"></div>

<!-- Argon Scripts -->
<!-- Core -->
<script src="{{ url('admin/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<!-- Optional JS -->
<script src="{{ url('admin/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ url('admin/vendor/chart.js/dist/Chart.extension.js') }}"></script>
<!-- Argon JS -->
<script src="{{ url('admin/js/argon.js?v=1.0.0') }}"></script>
</body>

</html>