<?php
use App\Category;

$categories = Category::where('status',1)->get();
?>

<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="{{ url('/') }}"><img src="{{ url('img/logo.png') }}" width="130px"></a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone">+63 966 777 8888</div>
						<div class="footer_contact_text">
							<p>17 Princess Road, London</p>
							<p>Grester London NW18JR, UK</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-2">
					<div class="footer_column">
						<div class="footer_title">Find your medicine</div>
						<ul class="footer_list">
							@foreach($categories as $category)
							<li><a href="{{ url('shop/*/'.$category->id) }}">{{ $category->description }}</a></li>
							@endforeach
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<div class="footer_title">Customer Care</div>
						<ul class="footer_list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order Tracking</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Customer Services</a></li>
							<li><a href="#">Returns / Exchange</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Product Support</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>

	<!-- Copyright -->

	<div class="copyright" style="background: #FD5F32;">
		<div class="container">
			<div class="row">
				<div class="col">
					
					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start" >
						<div class="copyright_content" style="color: #fff;"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Aluna
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
						<!-- <div class="logos ml-sm-auto">
							<ul class="logos_list">
								<li><a href="#"><img src="{{ url('guest/images/logos_1.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ url('guest/images/logos_2.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ url('guest/images/logos_3.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ url('guest/images/logos_4.png') }}" alt=""></a></li>
							</ul>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{{ url('guest/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ url('guest/styles/bootstrap4/popper.js') }}"></script>
<script src="{{ url('guest/styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{ url('guest/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ url('guest/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ url('guest/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ url('guest/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ url('guest/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ url('guest/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ url('guest/plugins/easing/easing.js') }}"></script>
<script src="{{ url('guest/plugins/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ url('guest/plugins/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>
<script src="{{ url('guest/plugins/parallax-js-master/parallax.min.js') }}"></script>

@if(Request::is('view-item/*'))
	<script src="{{ url('guest/js/product_custom.js') }}"></script>
@elseif(Request::is('shop') || Request::is('shop/*'))
	<script src="{{ url('guest/js/shop_custom.js') }}"></script>
@elseif(Request::is('my-cart'))
	<script src="{{ url('guest/js/cart_custom.js') }}"></script>
@endif



</body>

</html>