<?php
use App\Category;

$categories = Category::where('status',1)->get();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Aluna PH | UHACK 2019</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Aluna PH | UHACK 2019">

<!-- Favicon -->
<link href="{{ url('img/icon.png') }}" rel="icon" type="image/png">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/bootstrap4/bootstrap.min.css') }}">
<link href="{{ url('guest/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ url('guest/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('guest/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('guest/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('guest/plugins/jquery-ui-1.12.1.custom/jquery-ui.css') }}">

@if(Request::is('view-item/*'))
	<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/product_styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/product_responsive.css') }}">

@elseif(Request::is('shop') || Request::is('shop/*') || Request::is('customer-care') || Request::is('track-order'))
	<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/shop_styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/shop_responsive.css') }}">

@elseif(Request::is('my-cart'))
	<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/cart_styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('guest/styles/cart_responsive.css') }}">

@endif

</head>

<body>

<div class="super_container">
	
	<!-- Header -->
	
	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar" style="background: #FD5F32;">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<!-- <div class="top_bar_contact_item"><div class="top_bar_icon"><img src="guest/images/phone.png" alt=""></div>+38 068 005 3570</div> -->
						<div class="top_bar_contact_item"><div class="top_bar_icon"><img src="{{ url('guest/images/mail.png') }}" alt=""></div><a href="mailto:support@aluna.ph">support@aluna.ph</a></div>
						<div class="top_bar_content ml-auto">
							<div class="top_bar_menu">

								<!-- <ul class="standard_dropdown top_bar_dropdown">

									<li>
										<a href="#">$ US dollar<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li><a href="#">EUR Euro</a></li>
											<li><a href="#">GBP British Pound</a></li>
											<li><a href="#">JPY Japanese Yen</a></li>
										</ul>
									</li>

								</ul> -->

							</div>
							<div class="top_bar_user">
								<div class="user_icon"><img src="{{ url('guest/images/user.svg') }}" alt=""></div>
								@if(Auth::check())
									<div><a href="{{ url('my-account') }}">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</a></div>
									<div><a href="{{ url('logout') }}">Logout</a></div>
								@else
									<div><a href="{{ url('register') }}">Register</a></div>
									<div><a href="{{ url('login') }}">Sign in</a></div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<!-- Header Main -->

		<div class="header_main">
			<div class="container">
				<div class="row">

					<!-- Logo -->
					<div class="col-lg-2 col-sm-3 col-3 order-1">
						<div class="logo_container">
							<div class="logo"><a href="{{ url('/') }}"><img src="{{ url('img/logo.png') }}" width="150px"></a></div>
						</div>
					</div>

					<!-- Search -->
					<div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
						<div class="header_search">
							<div class="header_search_content">
								<div class="header_search_form_container">
									<form action="#" class="header_search_form clearfix">
										<input type="search" required="required" class="header_search_input" placeholder="Search for products...">
										<div class="custom_dropdown">
											<div class="custom_dropdown_list">
												<span class="custom_dropdown_placeholder clc">All Categories</span>
												<i class="fas fa-chevron-down"></i>
												<ul class="custom_list clc">
													<li><a class="clc" href="{{ url('shop/*') }}">All Categories</a></li>
													@foreach($categories as $category)
													<li><a class="clc" href="{{ url('shop/'.$category->id) }}">{{ $category->description }}</a></li>
													@endforeach
												</ul>
											</div>
										</div>
										<button type="submit" class="header_search_button trans_300" value="Submit"><img src="{{ url('guest/images/search.png') }}" alt=""></button>
									</form>
								</div>
							</div>
						</div>
					</div>

					<!-- Wishlist -->
					<div class="col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right">
						<div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
							<div class="wishlist d-flex flex-row align-items-center justify-content-end">
								<div class="wishlist_icon"><img src="{{ url('guest/images/heart.png') }}" alt=""></div>
								<div class="wishlist_content">
									<div class="wishlist_text"><a href="#">Wishlist</a></div>
									<div class="wishlist_count">0</div>
								</div>
							</div>

							<!-- Cart -->
							<div class="cart">
								<div class="cart_container d-flex flex-row align-items-center justify-content-end">
									<div class="cart_icon">
										<a href="{{ url('my-cart') }}">
										<img src="guest/images/cart.png" alt="">
										<div class="cart_count"><span>0</span></div>
										</a>
									</div>
									<div class="cart_content">

										@if(Auth::check())
											<div class="cart_text"><a href="{{ url('my-cart') }}">Cart</a></div>
											<div class="cart_price">₱ {{ number_format(0,2) }}</div>
										@else
											<div class="cart_text"><a href="{{ url('my-cart') }}">Cart</a></div>
											<div class="cart_price">₱ {{ number_format(0,2) }}</div>
										@endif

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col">
						
						<div class="main_nav_content d-flex flex-row">

							<!-- Categories Menu -->

							<div class="cat_menu_container">
								<div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
									<div class="cat_burger"><span></span><span></span><span></span></div>
									<div class="cat_menu_text">categories</div>
								</div>

								<ul class="cat_menu">
									@foreach($categories as $category)
									<li><a href="{{ url('shop/'.$category->id) }}">{{ $category->description }}<i class="fas fa-chevron-right"></i></a></li>
									@endforeach
									<!-- <li class="hassubs">
										<a href="#">Hardware<i class="fas fa-chevron-right"></i></a>
										<ul>
											<li class="hassubs">
												<a href="#">Menu Item<i class="fas fa-chevron-right"></i></a>
												<ul>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
													<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
												</ul>
											</li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
											<li><a href="#">Menu Item<i class="fas fa-chevron-right"></i></a></li>
										</ul>
									</li> -->

								</ul>
							</div>

							<!-- Main Nav Menu -->

							<div class="main_nav_menu ml-auto">
								<ul class="standard_dropdown main_nav_dropdown">
									<li><a href="{{ url('shop') }}" style="color: #000 !important;">Shop<i class="fas fa-chevron-down"></i></a></li>
									<li class="hassubs">
										<a href="#" style="color: #000 !important;">Top Deals<i class="fas fa-chevron-down"></i></a>
										<ul>
											@foreach($categories as $category)
											<li><a href="{{ url('shop/'.$category->id) }}">{{ $category->description }}<i class="fas fa-chevron-down"></i></a></li>
											@endforeach
										</ul>
									</li>

									<li><a href="{{ url('customer-care') }}" style="color: #000 !important;">Customer Care<i class="fas fa-chevron-down"></i></a></li>

									<li><a href="{{ url('track-order') }}" style="color: #000 !important;">Track My Order<i class="fas fa-chevron-down"></i></a></li>
									
								</ul>
							</div>

							<!-- Menu Trigger -->

							<div class="menu_trigger_container ml-auto">
								<div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
									<div class="menu_burger">
										<div class="menu_trigger_text">menu</div>
										<div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</nav>
