<?php

Route::get('/','GuestController@showHome');
Route::get('shop','GuestController@showShop');
Route::get('shop/{category_id}','GuestController@showShopCategory');

Route::get('view-item/{item_id}','GuestController@showViewItem');

Route::get('my-cart','GuestController@showMyCart');

Route::get('customer-care','GuestController@showCustomerCare');
Route::get('track-order','GuestController@showTrackOrder');

Route::post('add-to-cart','CartController@doAddToCart');

Route::get('add-to-list/{item_id}','QuotationsController@showAddToList');
Route::post('add-to-list','BiddingController@create');

Route::get('create-new-list','QuotationsController@showCreateNewList');
Route::post('create-new-list','QuotationsController@doCreateNewList');

// GUEST LOGIN
Route::get('login','GuestController@showLogin')->name('login');
Route::post('login','GuestController@doLogin');

Route::get('logout','GuestController@doLogout');

////////////////
// Quotations //
////////////////

Route::get('quotations', 'QuotationsController@showQuotations');
Route::get('quotations/{quotation}', 'QuotationsController@show');
Route::post('biddings/{quotation_id}','QuotationsController@showQuotationsBidding');

Route::post('start-timer/{quotation}', 'QuotationsController@startTimer');


Route::get('suppliers', 'ProductController@suppliers');

////////////////////
// Purchase Order //
////////////////////

Route::get('purchase-order', 'PurchaseOrderController@create');

// ===================ADMIN===================

// ADMIN LOGIN
Route::get('admin/login','GuestController@showAdminLogin');
Route::post('admin/login','GuestController@doAdminLogin');

Route::get('admin/dashboard','AdminController@showDashboard');

Route::get('admin/users','UserController@showUsers');
Route::get('admin/activate-user/{user_id}','UserController@doActivateUser');
Route::get('admin/deactivate-user/{user_id}','UserController@doDeactivateUser');

Route::get('admin/suppliers','SupplierController@showSuppliers');
Route::get('admin/activate-supplier/{supplier_id}','SupplierController@doActivateSupplier');
Route::get('admin/deactivate-supplier/{supplier_id}','SupplierController@doDeactivateSupplier');

Route::get('admin/items','ItemController@showItems');
Route::get('admin/activate-item/{item_id}','ItemController@doActivateItem');
Route::get('admin/deactivate-item/{item_id}','ItemController@doDeactivateItem');