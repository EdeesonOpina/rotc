<?php

namespace App;

use App\Item;
use App\User;
use App\Bidding;
use App\Traits\SavesQuotationNumber;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
	use SavesQuotationNumber;

	protected $fillable =  ['quotation_num', 'category_id', 'item_id', 'user_id', 'qty', 'start_timer', 'end_timer', 'complete_quotation_num', 'company_id', 'item_sales', 'warranty_years', 'estimated_time_days', 'cost_per_piece', 'total_cost'
    ];

    public function biddings()
    {
    	return $this->hasMany(Bidding::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function item()
    {
    	return $this->belongsTo(Item::class);
    }
}
