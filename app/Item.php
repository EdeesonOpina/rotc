<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable =  ['name', 'SKU', 'supplier_id', 'category_id', 'sub_category_id', 'description', 'units_sold', 'estimated_time_days', 'price',         
'category_id',    
'sub_category_id',
'description',    
'price',          
'image',          
'status'];
    //
    
    public function supplier()
    {
    	return $this->belongsTo(Supplier::class);
    }
}
