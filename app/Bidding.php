<?php

namespace App;

use App\Item;
use App\Product;
use App\Supplier;
use App\Quotation;
use Illuminate\Database\Eloquent\Model;

class Bidding extends Model
{
    protected $fillable =  ['quotation_id', 'supplier_id', 'product_id', 'price', 'total', 'status', 'estimated_time_days'
    ];

    public function quotation()
    {
    	return $this->belongsTo(Quotation::class);
    }

    // public function supplier()
    // {
    // 	return $this->belongsTo(Supplier::class());
    // }
    
    public function product()
    {
    	return $this->belongsTo(Item::class);
    }
}
