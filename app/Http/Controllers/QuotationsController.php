<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use App\Item;
use App\User;
use Validator;
use App\Bidding;
use App\Product;
use App\Supplier;
use App\Quotation;
use Illuminate\Http\Request;

class QuotationsController extends Controller
{
    public function showQuotations()
    {
        $quotations = Quotation::where('user_id', Auth::user()->id)->paginate(15);

        return view('accounts.quotations', compact(
            'quotations'
        ));
    }

    public function showQuotationsBidding($quotation_id){

        $biddings = Bidding::where('quotation_id',$quotation_id)->paginate(15);

        return view('accounts.biddings',compact(
            'biddings'
        ));

    }

    public function showAddToList($item_id)
    {
        $quotations = Quotation::where('company_id', Auth::user()->supplier_id)->get();
        $item = Item::find($item_id);

        // dd($item);

        return view('accounts.add_to_list',compact(
            'quotations',
            'item'
        ));
    }

    public function showCreateNewList()
    {
        return view('quotation.form');
    }

    public function doCreateNewList(Request $request){

        $item = Item::find($request->input('item_id'));

        $quotation = new Quotation;
        // $quotation->quotation_num = time().Auth::user()->id;
        // $quotation->complete_quotation_num = time().Auth::user()->id;
        // $quotation->full_quotation_num = time().Auth::user()->id;
        $quotation->category_id = $item->category_id;
        $quotation->item_id = $item->id;
        $quotation->user_id = Auth::user()->id;
        $quotation->company_id = Auth::user()->supplier_id;
        $quotation->qty = $request->qty;
        $quotation->save();

        $request->session()->flash('success', 'Great! You have created a new list.');

        return redirect('view-item/'.$item->id);

    }

    public function show(Quotation $quotation)
    {
        $biddings = Bidding::where('quotation_id', $quotation->id)->get();

        return view('quotation.quotation', compact('quotation', 'biddings'));


        // return ['status' => 'status', 'biddings' => $biddings];
    }

    public function startTimer(Quotation $quotation)
    {
        $quotation->start_timer = now();
        $quotation->save();

        return redirect('quotations/'.$quotation->id);
    }

    public function create(Request $request)
    {
        $quotation = Quotation::create([
            // 'quotation_num' => Quotation::newFullQuotationNumber(),
            'category_id' => $request->category_id,
            'item_id' => $request->item_id,
            'user_id' => Auth::id(),
            'company_id' => Auth::user()->supplier_id,
            'qty' => $request->qty,
            'start_timer' => Carbon::now(),
            'end_timer' => Carbon::now()->addHours(24),

            // prefered by buyer
            'years_joined' => $request->years_joined,
            'item_sales' => $request->item_sales,
            'warranty_years' => $request->warranty_years,
            'estimated_time_days' => $request->estimated_time_days,
            'cost_per_piece' => $request->cost_per_piece,
            'total_cost' => $request->qty * $request->total_cost,
        ]);

        return ['status' => 'success', 'quotation' => $quotation];
    }
}
