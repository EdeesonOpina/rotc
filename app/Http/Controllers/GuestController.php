<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use App\Cart;
use App\Item;
use App\User;
use Validator;
use App\Product;
use App\Category;
use App\SubCategory;
use App\Transaction;
use Illuminate\Http\Request;

class GuestController extends Controller
{

    public function showHome(){

    	$categories = Category::where('status',1)->get();

		$deals_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(3);

		$featured_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);
		$sale_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);
		$hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);

		$top_newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(1);
		$top_10_newest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_newest_items = Item::where('status',1)->get()->take(12);
		$newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(12);

		$top_hottest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(10);
		$top_10_hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_hottest_items = Item::where('status',1)->get()->take(12);

		return view('guests.home',compact(
			'top_hottest_items',
			'top_10_hottest_items',
			'cheap_hottest_items',
			'cheap_newest_items',
			'top_10_newest_items',
			'deals_items',
			'top_newest_items',
			'featured_items',
			'sale_items',
			'hottest_items',
			'newest_items',
			'categories'
		));

    }

    public function showShop(){

		$categories = Category::where('status',1)->get();

		$items = Item::where('status',1)->orderByRaw('rand()')->paginate(20);
		$total_items_count = Item::where('status',1)->count();

		$deals_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(3);

		$featured_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);
		$sale_items = Item::where('status',1)->get()->take(12);
		$hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);

		$top_newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(1);
		$top_10_newest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_newest_items = Item::where('status',1)->get()->take(12);
		$newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(12);

		$top_hottest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(10);
		$top_10_hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_hottest_items = Item::where('status',1)->get()->take(12);

		return view('guests.shop',compact(
			'items',
			'total_items_count',
			'top_hottest_items',
			'top_10_hottest_items',
			'cheap_hottest_items',
			'cheap_newest_items',
			'top_10_newest_items',
			'deals_items',
			'top_newest_items',
			'featured_items',
			'sale_items',
			'hottest_items',
			'newest_items',
			'categories'
		));

	}

	public function showShopCategory($category_id){

		$categories = Category::where('status',1)->get();

        $query = Item::where('status', 1);

        $items = $query->orderBy('created_at', 'desc')
            ->paginate(20);

        $total_items_count = $query->count();

		$deals_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(3);

		$featured_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);
		$sale_items = Item::where('status',1)->get()->take(12);
		$hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);

		$top_newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(1);
		$top_10_newest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_newest_items = Item::where('status',1)->orderBy('price','asc')->get()->take(12);
		$newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(12);

		$top_hottest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(10);
		$top_10_hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_hottest_items = Item::where('status',1)->orderBy('price','asc')->get()->take(12);

		return view('guests.shop',compact(
			'items',
			'total_items_count',
			'top_hottest_items',
			'top_10_hottest_items',
			'cheap_hottest_items',
			'cheap_newest_items',
			'top_10_newest_items',
			'deals_items',
			'top_newest_items',
			'featured_items',
			'sale_items',
			'hottest_items',
			'newest_items',
			'categories'
		));

	}

    public function showLogin(){

    	return view('guests.login');

    }

    public function showAdminLogin(){

    	return view('admins.login');

    }

    public function showMyCart(){

    	$carts = Cart::where('user_id',Auth::user()->id)->get();

    	$carts_total = Cart::where('user_id',Auth::user()->id)->sum('total');

    	return view('guests.my_cart',compact(
    		'carts_total',
    		'carts'
    	));

    }

    public function doLogin(Request $request){

		$rules = [
            'email'=>'required',
            'password'=>'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){

            return back()->withErrors($validator);

        }else{

			if(Auth::attempt([
				'email'=>$request->input('email'),
				'password'=>$request->input('password'),
			])){

				if(Auth::user()->status == '-1'){

					$request->session()->flash('error','Your account has been deactivated.');

					return redirect('logout');

				}else{

					return redirect('my-cart');

				}

			}else{

				$request->session()->flash('error','Invalid Username/Password');

				return back();
			}

		}

	}

    public function doAdminLogin(Request $request){

		$rules = [
            'email'=>'required',
            'password'=>'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){

            return back()->withErrors($validator);

        }else{

			if(Auth::attempt([
				'email'=>$request->input('email'),
				'password'=>$request->input('password'),
			])){

				if(Auth::user()->status == '-1'){

					$request->session()->flash('error','Your account has been deactivated.');

					return redirect('logout');

				}else{

					return redirect('admin/dashboard');

				}

			}else{

				$request->session()->flash('error','Invalid Username/Password');

				return back();
			}

		}

	}

	public function showViewItem($item_id)
	{
		$item = Item::find($item_id);

		$category = Category::find($item->category_id);

		$items = Item::where('status',1)->orderByRaw('rand()')->paginate(20);
		$total_items_count = Item::where('status',1)->count();

		$deals_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(3);

		$featured_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);
		$sale_items = Item::where('status',1)->get()->take(12);
		$hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(12);

		$top_newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(1);
		$top_10_newest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_newest_items = Item::where('status',1)->orderBy('price','asc')->get()->take(12);
		$newest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(12);

		$top_hottest_items = Item::where('status',1)->orderBy('created_at','desc')->get()->take(10);
		$top_10_hottest_items = Item::where('status',1)->orderByRaw('rand()')->get()->take(10);
		$cheap_hottest_items = Item::where('status',1)->orderBy('price','asc')->get()->take(12);

		$total = 0;

		return view('guests.view_item',compact(
			'item',
			'category',
			'total',
			'total_items_count',
			'top_hottest_items',
			'top_10_hottest_items',
			'cheap_hottest_items',
			'cheap_newest_items',
			'top_10_newest_items',
			'deals_items',
			'top_newest_items',
			'featured_items',
			'sale_items',
			'hottest_items',
			'newest_items'
		));

	}

	public function doLogout(){

		Auth::logout();

		return redirect('/');

	}

}
