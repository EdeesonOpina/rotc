<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use Mail;

use App\User;
use App\Item;
use App\Category;
use App\SubCategory;
use App\Transaction;
use App\Supplier;

class AdminController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function showDashboard(){

    	$items = Item::orderBy('created_at','desc')->get()->take(5);
        $suppliers = Supplier::orderBy('created_at','desc')->get()->take(10);
        $users = User::orderBy('created_at','desc')->get()->take(5);

        $items_total_count = Item::where('status',1)->count();
        $suppliers_total_count = Supplier::where('status',1)->count();
        $users_total_count = User::where('status',1)->count();
        $customers_total_count = User::where('status',1)->where('role','Customer')->count();

        return view('admins.dashboard',compact(
            'items_total_count',
            'suppliers_total_count',
            'users_total_count',
            'customers_total_count',
            'items',
            'suppliers',
            'users'
        ));

    }

}
