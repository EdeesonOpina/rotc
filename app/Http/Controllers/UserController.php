<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Mail;
use DB;
use Validator;

use App\User;
use App\Supplier;
use App\Item;

class UserController extends Controller
{
    
	public function showUsers(){

		$users = User::orderBy('created_at','desc')->paginate(10);

		$users_active_count = User::where('status',1)->count();
		$users_inactive_count = User::where('status','-1')->count();
		$users_pending_count = User::where('status',0)->count();
		$users_total_count = User::count();

		return view('admins.users.manage_users',compact(
			'users',
			'users_active_count',
			'users_inactive_count',
			'users_pending_count',
			'users_total_count'
		));

	}

	public function doActivateUser(Request $request,$user_id){

    	$user = User::find($user_id);

    	$user->status = 1;
    	$user->save();

    	$request->session()->flash('success','You have successfully activated this user.');

    	return back();

    }

    public function doDeactivateUser(Request $request,$user_id){

    	$user = User::find($user_id);

    	$user->status = '-1';
    	$user->save();

    	$request->session()->flash('success','You have successfully deactivated this user.');

    	return back();

    }

}
