<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use Mail;

use App\User;
use App\Item;
use App\Category;
use App\SubCategory;
use App\Transaction;
use App\Cart;

class CartController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function doAddToCart(Request $request){

    	$item = Item::find($request->input('item_id'));

    	$cart = new Cart;

    	$cart->item_id = $item->id;
    	$cart->user_id = Auth::user()->id;
    	$cart->category_id = $item->category_id;
    	$cart->qty = $request->input('qty');
    	$cart->total = $request->input('qty') * $item->price;
    	$cart->save();

    	return redirect('my-cart');

    }

    public function showMyCart(){

    	$carts = Cart::where('user_id',Auth::user()->id)->get();

    	$carts_total = Cart::where('user_id',Auth::user()->id)->sum('total');

    	return view('accounts.my_cart',compact(
    		'carts_total',
    		'carts'
    	));

    }

}
