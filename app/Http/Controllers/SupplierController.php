<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Mail;
use DB;
use Validator;

use App\User;
use App\Supplier;

class SupplierController extends Controller
{
    
	public function showSuppliers(){

		$suppliers = Supplier::orderBy('created_at','desc')->paginate(10);

		$suppliers_active_count = Supplier::where('status',1)->count();
		$suppliers_inactive_count = Supplier::where('status','-1')->count();
		$suppliers_pending_count = Supplier::where('status',0)->count();
		$suppliers_total_count = Supplier::count();

		return view('admins.suppliers.manage_suppliers',compact(
			'suppliers',
			'suppliers_active_count',
			'suppliers_inactive_count',
			'suppliers_pending_count',
			'suppliers_total_count'
		));

	}

	public function doActivateSupplier(Request $request,$supplier_id){

    	$supplier = Supplier::find($supplier_id);

    	$supplier->status = 1;
    	$supplier->save();

    	$request->session()->flash('success','You have successfully activated this supplier.');

    	return back();

    }

    public function doDeactivateSupplier(Request $request,$supplier_id){

    	$supplier = Supplier::find($supplier_id);

    	$supplier->status = '-1';
    	$supplier->save();

    	$request->session()->flash('success','You have successfully deactivated this supplier.');

    	return back();

    }

}
