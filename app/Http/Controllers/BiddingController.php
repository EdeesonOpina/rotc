<?php

namespace App\Http\Controllers;

use App\Item;
use App\Bidding;
use App\Product;
use App\Quotation;
use Illuminate\Http\Request;

class BiddingController extends Controller
{
    public function create(Request $request)
    {
        // dd($request->all());

        $quotation = Quotation::find($request->quotation_id);
        $product = Item::find($request->item_id);

        $bidding = Bidding::create([
            'quotation_id' => $quotation->id,
            'supplier_id' => $product->supplier_id,
            'product_id' => $product->id,
            'price' => $product->price,
            'total' => $product->price * $quotation->qty,
            'status' => 'active'
        ]);

        return redirect('quotations/' . $quotation->id);

        // return ['status' => 'success', 'bidding' => $bidding];
    }

    public function update(Request $request, Bidding $bidding)
    {
    	$updatedBidding = $bidding->update([
    	   'price' => $request->price,
           'total' => $request->price * $bidding->quotation->qty
    	]);

        return ['status' => 'success', 'bidding' => $updatedBidding];
    }
}
