<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function suppliers()
    {
    	$suppliers = Supplier::all();

    	// return $suppliers->toJson();

    	return $suppliers;
    }
}
