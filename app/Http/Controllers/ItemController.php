<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Validator;
use Mail;

use App\User;
use App\Item;

class ItemController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function showItems(){

		$items = Item::where('status',1)->orderBy('created_at','desc')->paginate(10);

		return view('admins.items.manage_items',compact(
			'items'
		));

	}

	public function doActivateItem(Request $request,$item_id){

    	$item = Item::find($item_id);

    	$item->status = 1;
    	$item->save();

    	$request->session()->flash('success','You have successfully activated this item.');

    	return back();

    }

    public function doDeactivateItem(Request $request,$item_id){

    	$item = Item::find($item_id);

    	$item->status = '-1';
    	$item->save();

    	$request->session()->flash('success','You have successfully deactivated this item.');

    	return back();

    }

}
