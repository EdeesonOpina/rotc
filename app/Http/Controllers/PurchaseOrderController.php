<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PurchaseOrderController extends Controller
{
	public function create()
	{
		return view('purchase_order.purchase_order_form');
	}

	public function save(Request $request)
    {
    	$auth = Auth::user();

        $purchaseOrder = PurchaseOrder::create([
        	'company_id' => $user->company_id,
        	'street_address' => $request->street_address ?? $company->street_address,
        	'city' => $request->city ?? $company->city,
        	'zip_code' => $request->zip_code ?? $company->zip_code,
        	'email' => $request->email ?? $company->email,
        	'user_id' => $user->id,
        	'date_filed' => $request->date_filed,
        	'contact_name' => $request->contact_name,
        	'contact_phone' => $request->contact_phone,
        	'contact_email' => $request->contact_email
        ]);

        $assignatories = $request->assignatories;

        foreach ($assignatories as $a) {
        	Assignatory::create([
        		'name' => $a->name,
        		'position' => $a->position,
        		'email' => $a->email,
        		'phone' => $a->phone,
        		'purchase_order_id' => $purchaseOrder->id,
        	]);
        }

        return ['status' => 'success', 'quotation' => $quotation];
    }
}
