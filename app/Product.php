<?php

namespace App;

use App\Supplier;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable =  ['name', 'SKU', 'supplier_id', 'category_id', 'sub_category_id', 'description', 'units_sold', 'estimated_time_days', 'price'];
    //
    
    public function supplier()
    {
    	return $this->belongsTo(Supplier::class);
    }
}
