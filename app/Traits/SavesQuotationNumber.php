<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

trait SavesQuotationNumber
{
    protected static function bootSavesQuotationNumber()
    {
        static::creating(function ($model) {
            $model->quotation_num = $model::newQuotationNumber();
            $model->full_quotation_num = $model::newFullQuotationNumber();
        });
    }

    // public function getFullFormNumberAttribute()
    // {
    //     return static::$modelInitial . '-' . config('goodsrequest.company_initial') . $this->created_at->year . str_pad($this->form_number, 6, 0, STR_PAD_LEFT);
    // }

    public static function newFullQuotationNumber()
    {
    	return date('Y') . str_pad(static::newQuotationNumber(), 6, 0, STR_PAD_LEFT);
    }

    public static function newQuotationNumber()
    {
    	// return static::where('company_id', Auth::user()->supplier_id)
    		// ->max('quotation_num') + 1;

        return static::max('quotation_num') + 1;
    }
}
