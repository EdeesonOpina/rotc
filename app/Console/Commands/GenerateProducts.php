<?php

namespace App\Console\Commands;

use App\Product;
use Faker\Factory;
use Illuminate\Console\Command;

class GenerateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = Factory::create();

        for ($i = 0; $i <= 10; $i++) {
            Product::create([
                'name' => 'Tisyu ' . rand(1,5),
                'SKU' => substr($faker->word, 5) . '0000' . rand(10, 99),
                'supplier_id' => 1,
                'category_id' => 1,
                'sub_category_id' => 1,
                'description' => $faker->text
            ]);
        }
    }
}
