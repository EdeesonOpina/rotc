<?php

namespace App\Console\Commands;

use App\Supplier;
use Faker\Factory;
use Illuminate\Console\Command;

class GenerateSupplier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:supplier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Fake Supplier';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = Factory::create();

        for ($i = 0; $i <= 5; $i++) {
            Supplier::create([
                'name' => $faker->company,
                'address' => $faker->address,
                'status' => 'verified',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
