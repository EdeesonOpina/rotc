<?php

namespace App\Console\Commands;

use App\Bidding;
use Faker\Factory;
use Illuminate\Console\Command;

class GenerateBiddings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:biddings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = Factory::create();

        for ($i = 0; $i <= 5; $i++) {
            Bidding::create([
                'quotation_id' => 1,
                'supplier_id' => rand(1,5),
                'product_id' => rand(1,10),
                'price' => rand(1, 10000),
                'total' => 0,
                'status' => 'active'
            ]);
        }
    }
}
