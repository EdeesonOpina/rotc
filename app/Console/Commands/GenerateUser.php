<?php

namespace App\Console\Commands;

use App\User;
use Faker\Factory;
use Illuminate\Console\Command;

class GenerateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = Factory::create();

        for ($i = 0; $i <= 5; $i++) {
            User::create([
                'firstname' => $faker->firstname,
                'lastname' => $faker->lastname,
                'supplier_id' => $i,
                'role' => $faker->jobTitle,
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' => bcrypt('123123123'),
                'status' => 'verified',
            ]);
        }
    }
}
