<?php

namespace App\Console\Commands;

use App\Quotation;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Console\Command;

class GenerateQuotations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:quotation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $faker = Factory::create();

        for ($i = 0; $i <= 5; $i++) {
            Quotation::create([
                'quotation_num' => $i,
                'category_id' => 1,
                'item_id' => 1,
                'user_id' => rand(1, 5),
                'qty' => rand(1, 100),
                'start_timer' => now(),
                'end_timer' => Carbon::now()->addHours(24),
                'years_joined' => rand(1, 10),
                'item_sales' => null,
                'warranty_years' => 1,
                'estimated_time_days' => 10,
                'cost_per_piece' => 100,
                'total_cost' => 20000,
            ]);
        }
    }
}
