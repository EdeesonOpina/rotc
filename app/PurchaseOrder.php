<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    $fillable = [
		'company_id',
    	'street_address',
    	'city',
    	'zip_code',
    	'email',
    	'user_id',
    	'date_filed',
    	'contact_name',
    	'contact_phone',
    	'contact_email'
    ];
}
