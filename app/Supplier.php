<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['name', 'address', 'status', 'created_at', 'updated_at'
    ];

    protected $casts = ['created_at', 'updated_at'];

    public function users()
    {
    	return $this->hasMany(User::class);
    }
}
