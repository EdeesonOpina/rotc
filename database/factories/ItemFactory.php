<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'category_id' => $faker->numberBetween($min = 1, $max = 8),
        'price' => $faker->numberBetween($min = 10, $max = 1000),
        'image' => $faker->imageUrl($width = 800, $height = 600, 'city'),
        'status' => 1,
    ];
});
