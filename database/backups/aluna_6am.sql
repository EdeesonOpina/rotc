-- MySQL dump 10.16  Distrib 10.1.21-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: localhost
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `biddings`
--

DROP TABLE IF EXISTS `biddings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biddings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estimated_time_days` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biddings`
--

LOCK TABLES `biddings` WRITE;
/*!40000 ALTER TABLE `biddings` DISABLE KEYS */;
INSERT INTO `biddings` VALUES (1,1,3,1,'621','0','active','2019-09-28 11:35:18','2019-09-28 12:15:21',10),(2,1,4,2,'3334','0','active','2019-09-28 11:35:19','2019-09-28 12:26:41',NULL),(3,1,5,3,'2067','0','active','2019-09-28 11:35:19','2019-09-28 12:27:10',NULL),(4,2,3,1,'6020','0','active','2019-09-28 11:35:19','2019-09-28 12:21:22',NULL),(5,2,4,1,'4336','0','active','2019-09-28 11:35:19','2019-09-28 12:27:48',NULL),(6,2,5,1,'1180','0','active','2019-09-28 11:35:19','2019-09-28 12:28:00',NULL),(7,1,3,6,'501','0','active','2019-09-28 13:34:15','2019-09-28 13:34:15',NULL),(8,3,3,6,'501','1002000','active','2019-09-28 13:35:41','2019-09-28 13:35:41',NULL),(9,3,3,6,'501','1002000','active','2019-09-28 13:36:55','2019-09-28 13:36:55',NULL),(10,3,3,6,'501','1002000','active','2019-09-28 13:39:13','2019-09-28 13:39:13',NULL);
/*!40000 ALTER TABLE `biddings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts`
--

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
INSERT INTO `carts` VALUES (1,8,NULL,93,1,NULL,1,'978','2019-09-28 13:26:18','2019-09-28 13:26:18');
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Carpet','1','2019-09-28 12:44:28','2019-09-28 12:44:28'),(2,'Tiles','1','2019-09-28 12:44:28','2019-09-28 12:44:28'),(3,'Office Supplies','1','2019-09-28 12:44:28','2019-09-28 12:44:28'),(4,'Tools','1','2019-09-28 12:44:28','2019-09-28 12:44:28'),(5,'Lighting','1','2019-09-28 12:44:28','2019-09-28 12:44:28'),(6,'Toiletries','1','2019-09-28 12:44:29','2019-09-28 12:44:29'),(7,'Plumbing','1','2019-09-28 12:44:29','2019-09-28 12:44:29'),(8,'Car Parts','1','2019-09-28 12:44:29','2019-09-28 12:44:29');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_approvers`
--

DROP TABLE IF EXISTS `company_approvers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_approvers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_approvers`
--

LOCK TABLES `company_approvers` WRITE;
/*!40000 ALTER TABLE `company_approvers` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_approvers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deductions`
--

DROP TABLE IF EXISTS `deductions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deductions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deductions`
--

LOCK TABLES `deductions` WRITE;
/*!40000 ALTER TABLE `deductions` DISABLE KEYS */;
/*!40000 ALTER TABLE `deductions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Macejkovic PLC',5,NULL,'So they got their tails fast in their mouths--and they\'re all over with diamonds, and walked a little nervous about it just now.\' \'It\'s the thing Mock Turtle with a round face, and was coming to.','710','https://lorempixel.com/800/600/city/?56573','1','2019-09-28 12:44:29','2019-09-28 12:44:29'),(2,'Hagenes Group',2,NULL,'After a while she remembered the number of cucumber-frames there must be!\' thought Alice. One of the court. All this time the Queen say only yesterday you deserved to be otherwise than what it was.','273','https://lorempixel.com/800/600/city/?47318','1','2019-09-28 12:44:29','2019-09-28 12:44:29'),(3,'Lemke-Pollich',3,NULL,'The Knave shook his head sadly. \'Do I look like it?\' he said, turning to Alice. \'What IS a Caucus-race?\' said Alice; \'living at the Gryphon replied very gravely. \'What else had you to death.\"\' \'You.','888','https://lorempixel.com/800/600/city/?61115','1','2019-09-28 12:44:29','2019-09-28 12:44:29'),(4,'Fadel-Kuhn',7,NULL,'Dodo. Then they all crowded round her, about the whiting!\' \'Oh, as to the puppy; whereupon the puppy began a series of short charges at the Queen, and Alice, were in custody and under sentence of.','666','https://lorempixel.com/800/600/city/?48954','1','2019-09-28 12:44:30','2019-09-28 12:44:30'),(5,'O\'Conner LLC',8,NULL,'Duchess, \'and that\'s the queerest thing about it.\' (The jury all brightened up at the bottom of a tree a few minutes, and she sat on, with closed eyes, and feebly stretching out one paw, trying to.','209','https://lorempixel.com/800/600/city/?24134','1','2019-09-28 12:44:30','2019-09-28 12:44:30'),(6,'Gulgowski PLC',3,NULL,'Alice: \'allow me to introduce some other subject of conversation. While she was beginning to get dry very soon. \'Ahem!\' said the Duchess, \'chop off her head!\' Alice glanced rather anxiously at the.','190','https://lorempixel.com/800/600/city/?62820','1','2019-09-28 12:44:30','2019-09-28 12:44:30'),(7,'Kuhn Ltd',1,NULL,'Mock Turtle in a tone of great dismay, and began to feel a little while, however, she waited for a baby: altogether Alice did not seem to dry me at home! Why, I haven\'t had a large fan in the air.','346','https://lorempixel.com/800/600/city/?86328','1','2019-09-28 12:44:30','2019-09-28 12:44:30'),(8,'Pollich-Sporer',6,NULL,'Alice loudly. \'The idea of the jurymen. \'It isn\'t a bird,\' Alice remarked. \'Oh, you can\'t be civil, you\'d better ask HER about it.\' (The jury all looked puzzled.) \'He must have a prize herself, you.','914','https://lorempixel.com/800/600/city/?95601','1','2019-09-28 12:44:30','2019-09-28 12:44:30'),(9,'Anderson Inc',2,NULL,'Queen added to one of them.\' In another minute the whole pack of cards, after all. I needn\'t be so stingy about it, you know--\' \'What did they draw?\' said Alice, rather alarmed at the Hatter, \'you.','770','https://lorempixel.com/800/600/city/?58527','1','2019-09-28 12:44:30','2019-09-28 12:44:30'),(10,'Beahan-Hauck',2,NULL,'In a minute or two, they began solemnly dancing round and swam slowly back to the three gardeners, but she heard it muttering to himself as he spoke. \'UNimportant, of course, to begin with,\' said.','908','https://lorempixel.com/800/600/city/?32410','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(11,'Glover-Gulgowski',2,NULL,'Duchess: \'and the moral of that is--\"The more there is of yours.\"\' \'Oh, I BEG your pardon!\' cried Alice (she was obliged to write this down on one of the lefthand bit of stick, and held out its arms.','270','https://lorempixel.com/800/600/city/?22857','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(12,'Schoen, Berge and Stiedemann',8,NULL,'Tortoise--\' \'Why did they draw the treacle from?\' \'You can draw water out of the Lobster Quadrille, that she had asked it aloud; and in despair she put her hand in hand, in couples: they were.','618','https://lorempixel.com/800/600/city/?32378','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(13,'Brakus and Sons',4,NULL,'Oh dear! I wish I hadn\'t begun my tea--not above a week or so--and what with the day of the month, and doesn\'t tell what o\'clock it is!\' \'Why should it?\' muttered the Hatter. \'Does YOUR watch tell.','699','https://lorempixel.com/800/600/city/?95722','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(14,'Towne, Price and Abshire',3,NULL,'Why, she\'ll eat a bat?\' when suddenly, thump! thump! down she came upon a little bird as soon as look at them--\'I wish they\'d get the trial done,\' she thought, and it sat for a minute or two to.','165','https://lorempixel.com/800/600/city/?55111','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(15,'Pacocha-Altenwerth',1,NULL,'And will talk in contemptuous tones of the court and got behind him, and very angrily. \'A knot!\' said Alice, in a loud, indignant voice, but she heard her voice sounded hoarse and strange, and the.','178','https://lorempixel.com/800/600/city/?64648','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(16,'Barrows-Keeling',2,NULL,'I THINK; or is it twelve? I--\' \'Oh, don\'t talk about cats or dogs either, if you please! \"William the Conqueror, whose cause was favoured by the pope, was soon left alone. \'I wish the creatures.','725','https://lorempixel.com/800/600/city/?48331','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(17,'McCullough Inc',4,NULL,'Writhing, of course, to begin with; and being so many lessons to learn! No, I\'ve made up my mind about it; and while she remembered that she had not gone (We know it was empty: she did not like the.','191','https://lorempixel.com/800/600/city/?81998','1','2019-09-28 12:44:31','2019-09-28 12:44:31'),(18,'Reynolds and Sons',5,NULL,'Dormouse denied nothing, being fast asleep. \'After that,\' continued the Hatter, \'you wouldn\'t talk about trouble!\' said the Duchess, \'chop off her knowledge, as there was not a regular rule: you.','489','https://lorempixel.com/800/600/city/?15470','1','2019-09-28 12:44:32','2019-09-28 12:44:32'),(19,'Abshire, Schamberger and Homenick',6,NULL,'I\'ve had such a thing. After a while, finding that nothing more to come, so she went on, \'that they\'d let Dinah stop in the distance, sitting sad and lonely on a three-legged stool in the middle of.','738','https://lorempixel.com/800/600/city/?49461','1','2019-09-28 12:44:32','2019-09-28 12:44:32'),(20,'Fritsch Ltd',1,NULL,'Gryphon, and the White Rabbit read out, at the mushroom (she had kept a piece of rudeness was more hopeless than ever: she sat on, with closed eyes, and half of anger, and tried to speak, but for a.','447','https://lorempixel.com/800/600/city/?49082','1','2019-09-28 12:44:32','2019-09-28 12:44:32'),(21,'Treutel, Rohan and Mitchell',7,NULL,'I shall ever see you any more!\' And here poor Alice began in a large canvas bag, which tied up at this moment Alice appeared, she was talking. \'How CAN I have done that?\' she thought. \'I must be.','209','https://lorempixel.com/800/600/city/?25831','1','2019-09-28 12:44:32','2019-09-28 12:44:32'),(22,'Feil, Windler and Padberg',5,NULL,'While she was in livery: otherwise, judging by his face only, she would keep, through all her wonderful Adventures, till she had felt quite strange at first; but she gained courage as she could.','688','https://lorempixel.com/800/600/city/?69753','1','2019-09-28 12:44:32','2019-09-28 12:44:32'),(23,'Lowe Group',5,NULL,'AND SHOES.\' the Gryphon repeated impatiently: \'it begins \"I passed by his face only, she would manage it. \'They were obliged to have changed since her swim in the distance, and she thought it over.','996','https://lorempixel.com/800/600/city/?99498','1','2019-09-28 12:44:32','2019-09-28 12:44:32'),(24,'Bergnaum-Padberg',7,NULL,'Conqueror, whose cause was favoured by the hand, it hurried off, without waiting for turns, quarrelling all the arches are gone from this side of the accident, all except the Lizard, who seemed too.','964','https://lorempixel.com/800/600/city/?21793','1','2019-09-28 12:44:33','2019-09-28 12:44:33'),(25,'Kirlin, Gaylord and Franecki',4,NULL,'Multiplication Table doesn\'t signify: let\'s try Geography. London is the capital of Paris, and Paris is the use of a candle is like after the others. \'We must burn the house down!\' said the.','616','https://lorempixel.com/800/600/city/?20753','1','2019-09-28 12:44:33','2019-09-28 12:44:33'),(26,'McKenzie, Sawayn and King',2,NULL,'Alice, she went on, yawning and rubbing its eyes, for it was sneezing on the ground near the right size, that it would like the look of the house, and have next to her. The Cat only grinned a little.','89','https://lorempixel.com/800/600/city/?62824','1','2019-09-28 12:44:33','2019-09-28 12:44:33'),(27,'Dicki Group',3,NULL,'HATED cats: nasty, low, vulgar things! Don\'t let him know she liked them best, For this must be a grin, and she tried to look over their slates; \'but it doesn\'t mind.\' The table was a good deal to.','610','https://lorempixel.com/800/600/city/?54301','1','2019-09-28 12:44:33','2019-09-28 12:44:33'),(28,'Klein-Johnston',4,NULL,'I think--\' (for, you see, Miss, we\'re doing our best, afore she comes, to--\' At this moment Five, who had been (Before she had got burnt, and eaten up by two guinea-pigs, who were giving it a minute.','682','https://lorempixel.com/800/600/city/?67977','1','2019-09-28 12:44:33','2019-09-28 12:44:33'),(29,'Parker-Kessler',8,NULL,'Alice, swallowing down her flamingo, and began staring at the Gryphon replied rather crossly: \'of course you know about it, you know--\' (pointing with his whiskers!\' For some minutes it puffed away.','186','https://lorempixel.com/800/600/city/?84258','1','2019-09-28 12:44:34','2019-09-28 12:44:34'),(30,'Prohaska Ltd',2,NULL,'I\'m I, and--oh dear, how puzzling it all seemed quite dull and stupid for life to go on. \'And so these three little sisters,\' the Dormouse crossed the court, without even waiting to put it more.','988','https://lorempixel.com/800/600/city/?59617','1','2019-09-28 12:44:34','2019-09-28 12:44:34'),(31,'Feest, Boyer and Thompson',5,NULL,'King added in an agony of terror. \'Oh, there goes his PRECIOUS nose\'; as an explanation. \'Oh, you\'re sure to make personal remarks,\' Alice said very politely, \'if I had not gone (We know it was.','284','https://lorempixel.com/800/600/city/?80542','1','2019-09-28 12:44:35','2019-09-28 12:44:35'),(32,'Yundt Inc',2,NULL,'Alice began telling them her adventures from the trees had a door leading right into a line along the passage into the sky all the while, and fighting for the fan and gloves. \'How queer it seems,\'.','356','https://lorempixel.com/800/600/city/?38606','1','2019-09-28 12:44:35','2019-09-28 12:44:35'),(33,'Reynolds, Towne and Adams',1,NULL,'Digging for apples, indeed!\' said the Hatter. \'Does YOUR watch tell you more than that, if you please! \"William the Conqueror, whose cause was favoured by the soldiers, who of course had to fall.','420','https://lorempixel.com/800/600/city/?47810','1','2019-09-28 12:44:36','2019-09-28 12:44:36'),(34,'Satterfield Inc',6,NULL,'Don\'t let me help to undo it!\' \'I shall be punished for it to be nothing but a pack of cards: the Knave of Hearts, and I don\'t know the way down one side and then nodded. \'It\'s no use going back to.','566','https://lorempixel.com/800/600/city/?42787','1','2019-09-28 12:44:36','2019-09-28 12:44:36'),(35,'Streich LLC',4,NULL,'Panther received knife and fork with a little irritated at the mushroom (she had kept a piece of it in time,\' said the March Hare, who had not a moment that it led into the sky. Twinkle, twinkle--\"\'.','403','https://lorempixel.com/800/600/city/?50141','1','2019-09-28 12:44:36','2019-09-28 12:44:36'),(36,'Brakus-Hintz',1,NULL,'Alice replied in an encouraging opening for a good deal worse off than before, as the whole thing very absurd, but they were mine before. If I or she should meet the real Mary Ann, and be turned out.','176','https://lorempixel.com/800/600/city/?40753','1','2019-09-28 12:44:36','2019-09-28 12:44:36'),(37,'Reichert-Klein',7,NULL,'I\'ll stay down here! It\'ll be no chance of her knowledge. \'Just think of nothing better to say than his first speech. \'You should learn not to be a comfort, one way--never to be a footman because he.','576','https://lorempixel.com/800/600/city/?71231','1','2019-09-28 12:44:37','2019-09-28 12:44:37'),(38,'Bashirian-Kiehn',8,NULL,'I breathe\"!\' \'It IS a long and a pair of white kid gloves: she took courage, and went back to the Queen, the royal children, and everybody else. \'Leave off that!\' screamed the Gryphon. \'I mean, what.','890','https://lorempixel.com/800/600/city/?98122','1','2019-09-28 12:44:37','2019-09-28 12:44:37'),(39,'Klocko, Hayes and Dare',3,NULL,'March Hare. \'Yes, please do!\' pleaded Alice. \'And ever since that,\' the Hatter said, turning to Alice as he fumbled over the fire, stirring a large mustard-mine near here. And the Gryphon repeated.','580','https://lorempixel.com/800/600/city/?87222','1','2019-09-28 12:44:37','2019-09-28 12:44:37'),(40,'Cassin-Wisoky',5,NULL,'COULD grin.\' \'They all can,\' said the cook. The King turned pale, and shut his note-book hastily. \'Consider your verdict,\' he said to herself; \'the March Hare said to Alice. \'Nothing,\' said Alice.','983','https://lorempixel.com/800/600/city/?59202','1','2019-09-28 12:44:37','2019-09-28 12:44:37'),(41,'Kovacek-Hills',8,NULL,'WOULD put their heads off?\' shouted the Queen was to eat the comfits: this caused some noise and confusion, as the doubled-up soldiers were silent, and looked along the course, here and there. There.','76','https://lorempixel.com/800/600/city/?62087','1','2019-09-28 12:44:37','2019-09-28 12:44:37'),(42,'Mraz-Turcotte',6,NULL,'I might venture to go and take it away!\' There was a queer-shaped little creature, and held it out to her chin upon Alice\'s shoulder, and it said in an offended tone. And the muscular strength.','146','https://lorempixel.com/800/600/city/?62094','1','2019-09-28 12:44:38','2019-09-28 12:44:38'),(43,'Leannon Ltd',4,NULL,'Pat, what\'s that in the sea, some children digging in the sky. Alice went on, half to itself, half to herself, in a great deal of thought, and rightly too, that very few little girls eat eggs quite.','918','https://lorempixel.com/800/600/city/?94036','1','2019-09-28 12:44:38','2019-09-28 12:44:38'),(44,'Koss, Hoppe and Pouros',1,NULL,'I suppose?\' \'Yes,\' said Alice to find my way into that lovely garden. I think that very few things indeed were really impossible. There seemed to quiver all over with diamonds, and walked a little.','606','https://lorempixel.com/800/600/city/?84951','1','2019-09-28 12:44:38','2019-09-28 12:44:38'),(45,'Howell PLC',5,NULL,'Duchess to play croquet.\' Then they both bowed low, and their curls got entangled together. Alice was very uncomfortable, and, as the game was in the pool a little sharp bark just over her head to.','98','https://lorempixel.com/800/600/city/?99035','1','2019-09-28 12:44:39','2019-09-28 12:44:39'),(46,'Beatty PLC',3,NULL,'CURTSEYING as you\'re falling through the wood. \'If it had some kind of serpent, that\'s all the way down one side and then treading on her toes when they arrived, with a trumpet in one hand and a sad.','709','https://lorempixel.com/800/600/city/?71960','1','2019-09-28 12:44:39','2019-09-28 12:44:39'),(47,'Schumm-Shanahan',4,NULL,'AND SHOES.\' the Gryphon went on, spreading out the answer to shillings and pence. \'Take off your hat,\' the King said to the Queen, the royal children, and make one repeat lessons!\' thought Alice; \'I.','400','https://lorempixel.com/800/600/city/?63405','1','2019-09-28 12:44:39','2019-09-28 12:44:39'),(48,'Bradtke-Russel',8,NULL,'The Mouse did not appear, and after a fashion, and this was her dream:-- First, she tried to open her mouth; but she felt unhappy. \'It was the White Rabbit, \'but it doesn\'t mind.\' The table was a.','381','https://lorempixel.com/800/600/city/?83143','1','2019-09-28 12:44:39','2019-09-28 12:44:39'),(49,'Hackett Group',1,NULL,'March Hare, \'that \"I breathe when I was thinking I should understand that better,\' Alice said very humbly; \'I won\'t interrupt again. I dare say you never had fits, my dear, and that he had never.','344','https://lorempixel.com/800/600/city/?18494','1','2019-09-28 12:44:39','2019-09-28 12:44:39'),(50,'Sauer-Bashirian',4,NULL,'Crab took the hookah out of breath, and said nothing. \'Perhaps it doesn\'t matter a bit,\' she thought it would like the three were all talking at once, with a sigh: \'it\'s always tea-time, and we\'ve.','363','https://lorempixel.com/800/600/city/?77660','1','2019-09-28 12:44:40','2019-09-28 12:44:40'),(51,'Ziemann Ltd',1,NULL,'SOMEBODY ought to go with the day of the water, and seemed to think to herself, rather sharply; \'I advise you to set about it; if I\'m Mabel, I\'ll stay down here till I\'m somebody else\"--but, oh.','222','https://lorempixel.com/800/600/city/?36469','1','2019-09-28 12:44:40','2019-09-28 12:44:40'),(52,'Schroeder and Sons',2,NULL,'Alice looked up, and began whistling. \'Oh, there\'s no use in crying like that!\' said Alice timidly. \'Would you tell me,\' said Alice, always ready to play with, and oh! ever so many out-of-the-way.','707','https://lorempixel.com/800/600/city/?31554','1','2019-09-28 12:44:40','2019-09-28 12:44:40'),(53,'Douglas-Schaden',6,NULL,'I do it again and again.\' \'You are old,\' said the Queen. \'Sentence first--verdict afterwards.\' \'Stuff and nonsense!\' said Alice desperately: \'he\'s perfectly idiotic!\' And she went out, but it makes.','74','https://lorempixel.com/800/600/city/?81070','1','2019-09-28 12:44:41','2019-09-28 12:44:41'),(54,'Mills Inc',4,NULL,'And concluded the banquet--] \'What IS the fun?\' said Alice. \'Call it what you would seem to dry me at home! Why, I wouldn\'t be in before the trial\'s over!\' thought Alice. \'Now we shall have somebody.','781','https://lorempixel.com/800/600/city/?90899','1','2019-09-28 12:44:41','2019-09-28 12:44:41'),(55,'Purdy, Harvey and Jacobi',2,NULL,'Queen jumped up on to her usual height. It was as much as she spoke. (The unfortunate little Bill had left off writing on his knee, and looking anxiously about as it happens; and if it had some kind.','374','https://lorempixel.com/800/600/city/?70447','1','2019-09-28 12:44:41','2019-09-28 12:44:41'),(56,'Muller-Schulist',6,NULL,'King, with an M?\' said Alice. \'Anything you like,\' said the Queen, stamping on the twelfth?\' Alice went on planning to herself \'It\'s the thing Mock Turtle sang this, very slowly and sadly:-- \'\"Will.','419','https://lorempixel.com/800/600/city/?17705','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(57,'Mann-Harris',5,NULL,'Oh, I shouldn\'t want YOURS: I don\'t understand. Where did they draw the treacle from?\' \'You can draw water out of sight; and an old conger-eel, that used to it!\' pleaded poor Alice began to say it.','426','https://lorempixel.com/800/600/city/?14467','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(58,'Hill and Sons',2,NULL,'Duchess asked, with another dig of her head on her face brightened up at the top of it. She stretched herself up closer to Alice\'s side as she came up to the part about her any more questions about.','720','https://lorempixel.com/800/600/city/?53720','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(59,'Ziemann-Denesik',3,NULL,'Alice, and she was beginning to end,\' said the March Hare. Alice sighed wearily. \'I think you could only see her. She is such a subject! Our family always HATED cats: nasty, low, vulgar things!.','78','https://lorempixel.com/800/600/city/?88782','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(60,'Senger Inc',1,NULL,'Alice. \'I\'ve so often read in the sun. (IF you don\'t know what it was: she was up to the Mock Turtle yet?\' \'No,\' said the King said, for about the temper of your flamingo. Shall I try the.','221','https://lorempixel.com/800/600/city/?82205','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(61,'Cruickshank-Weimann',5,NULL,'Dormouse. \'Write that down,\' the King said, turning to the Knave of Hearts, carrying the King\'s crown on a three-legged stool in the house before she found it advisable--\"\' \'Found WHAT?\' said the.','701','https://lorempixel.com/800/600/city/?13324','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(62,'Kunde, Sanford and Graham',1,NULL,'Take your choice!\' The Duchess took no notice of them hit her in a thick wood. \'The first thing she heard something splashing about in the world am I? Ah, THAT\'S the great wonder is, that I\'m.','116','https://lorempixel.com/800/600/city/?41408','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(63,'Kunze, Jaskolski and Schroeder',3,NULL,'Knave of Hearts, he stole those tarts, And took them quite away!\' \'Consider your verdict,\' the King said, turning to the table for it, while the Mock Turtle is.\' \'It\'s the thing yourself, some.','72','https://lorempixel.com/800/600/city/?88708','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(64,'Zulauf, Turcotte and Haley',5,NULL,'The table was a table, with a large fan in the long hall, and close to the jury. They were indeed a queer-looking party that assembled on the bank, with her face like the Mock Turtle, suddenly.','334','https://lorempixel.com/800/600/city/?19554','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(65,'Block Inc',3,NULL,'Alice said to a mouse: she had peeped into the roof bear?--Mind that loose slate--Oh, it\'s coming down! Heads below!\' (a loud crash)--\'Now, who did that?--It was Bill, I fancy--Who\'s to go after.','498','https://lorempixel.com/800/600/city/?35450','1','2019-09-28 12:44:42','2019-09-28 12:44:42'),(66,'Nicolas, Witting and Reilly',7,NULL,'The Queen turned crimson with fury, and, after folding his arms and frowning at the moment, \'My dear! I shall have some fun now!\' thought Alice. \'Now we shall have to fly; and the bright eager eyes.','50','https://lorempixel.com/800/600/city/?13967','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(67,'Parker, Bode and Wiza',3,NULL,'It\'s by far the most curious thing I ever heard!\' \'Yes, I think it was,\' he said. \'Fifteenth,\' said the Duchess, it had no pictures or conversations in it, and they all moved off, and she felt.','712','https://lorempixel.com/800/600/city/?36680','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(68,'Yost, Mertz and Nicolas',5,NULL,'After a time there were three gardeners who were all turning into little cakes as they came nearer, Alice could not help thinking there MUST be more to do it! Oh dear! I\'d nearly forgotten to ask.\'.','131','https://lorempixel.com/800/600/city/?44495','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(69,'Rolfson-O\'Conner',6,NULL,'And mentioned me to sell you a couple?\' \'You are old,\' said the Caterpillar. \'I\'m afraid I\'ve offended it again!\' For the Mouse replied rather crossly: \'of course you know the way down one side and.','81','https://lorempixel.com/800/600/city/?69474','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(70,'Labadie and Sons',7,NULL,'And argued each case with my wife; And the moral of that is--\"Be what you like,\' said the Mouse, frowning, but very politely: \'Did you speak?\' \'Not I!\' said the Caterpillar. \'Well, I\'ve tried banks.','944','https://lorempixel.com/800/600/city/?24478','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(71,'Bergstrom Group',7,NULL,'I\'m afraid, but you might knock, and I shall never get to twenty at that rate! However, the Multiplication Table doesn\'t signify: let\'s try Geography. London is the capital of Rome, and Rome--no.','791','https://lorempixel.com/800/600/city/?42038','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(72,'Breitenberg, Muller and Muller',1,NULL,'Dormouse. \'Write that down,\' the King said to the executioner: \'fetch her here.\' And the executioner myself,\' said the Gryphon, the squeaking of the pack, she could have been changed for Mabel! I\'ll.','530','https://lorempixel.com/800/600/city/?48051','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(73,'Raynor, Mayert and Beier',8,NULL,'Cat, and vanished again. Alice waited patiently until it chose to speak again. The Mock Turtle with a great crash, as if he were trying which word sounded best. Some of the Gryphon, and, taking.','173','https://lorempixel.com/800/600/city/?16271','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(74,'Leannon, Koss and Schulist',3,NULL,'They\'re dreadfully fond of pretending to be seen: she found she had plenty of time as she spoke, but no result seemed to Alice for some time busily writing in his note-book, cackled out \'Silence!\'.','110','https://lorempixel.com/800/600/city/?66045','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(75,'Gleichner, Simonis and Schowalter',1,NULL,'Alice said to Alice, flinging the baby joined):-- \'Wow! wow! wow!\' \'Here! you may stand down,\' continued the Hatter, with an M, such as mouse-traps, and the whole she thought at first she thought it.','577','https://lorempixel.com/800/600/city/?73225','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(76,'Deckow LLC',5,NULL,'Mock Turtle sighed deeply, and drew the back of one flapper across his eyes. He looked at each other for some time in silence: at last came a rumbling of little Alice and all that,\' he said in a.','238','https://lorempixel.com/800/600/city/?32147','1','2019-09-28 12:44:43','2019-09-28 12:44:43'),(77,'Ratke LLC',4,NULL,'Lobster Quadrille?\' the Gryphon never learnt it.\' \'Hadn\'t time,\' said the Rabbit whispered in reply, \'for fear they should forget them before the officer could get to the Gryphon. \'Well, I hardly.','107','https://lorempixel.com/800/600/city/?39789','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(78,'Balistreri-Deckow',4,NULL,'The Mouse gave a little shaking among the trees, a little door was shut again, and said, \'So you think you\'re changed, do you?\' \'I\'m afraid I am, sir,\' said Alice; \'I can\'t go no lower,\' said the.','612','https://lorempixel.com/800/600/city/?92582','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(79,'Ziemann, Ratke and Windler',4,NULL,'She went in without knocking, and hurried upstairs, in great fear lest she should chance to be done, I wonder?\' And here Alice began in a moment: she looked down at her as hard as it spoke. \'As wet.','417','https://lorempixel.com/800/600/city/?49953','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(80,'Prohaska Group',2,NULL,'Rabbit coming to look down and began whistling. \'Oh, there\'s no use their putting their heads downward! The Antipathies, I think--\' (for, you see, so many different sizes in a great crash, as if she.','291','https://lorempixel.com/800/600/city/?77847','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(81,'Kozey-Gislason',3,NULL,'Rabbit\'s little white kid gloves while she remembered trying to box her own children. \'How should I know?\' said Alice, \'we learned French and music.\' \'And washing?\' said the King. \'Then it doesn\'t.','185','https://lorempixel.com/800/600/city/?96998','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(82,'Wisozk, Hauck and Mertz',4,NULL,'Do come back in a low, timid voice, \'If you do. I\'ll set Dinah at you!\' There was a long and a Long Tale They were just beginning to end,\' said the March Hare interrupted, yawning. \'I\'m getting.','399','https://lorempixel.com/800/600/city/?92096','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(83,'Schuppe PLC',4,NULL,'Please, Ma\'am, is this New Zealand or Australia?\' (and she tried her best to climb up one of the thing yourself, some winter day, I will tell you what year it is?\' \'Of course it is,\' said the cook.','543','https://lorempixel.com/800/600/city/?52877','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(84,'Kilback, Sipes and Wehner',3,NULL,'And certainly there was nothing else to do, and perhaps as this is May it won\'t be raving mad--at least not so mad as it went, as if it began ordering people about like mad things all this time. \'I.','673','https://lorempixel.com/800/600/city/?85664','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(85,'Nicolas PLC',1,NULL,'However, I\'ve got to the Cheshire Cat sitting on the other arm curled round her head. \'If I eat one of the song. \'What trial is it?\' \'Why,\' said the Cat. \'I said pig,\' replied Alice; \'and I wish you.','670','https://lorempixel.com/800/600/city/?53647','1','2019-09-28 12:44:44','2019-09-28 12:44:44'),(86,'McCullough PLC',2,NULL,'Dormouse indignantly. However, he consented to go nearer till she heard a little more conversation with her head!\' the Queen ordering off her knowledge, as there was silence for some time with the.','868','https://lorempixel.com/800/600/city/?14132','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(87,'Rau, Orn and Osinski',4,NULL,'Sing her \"Turtle Soup,\" will you, won\'t you, will you, won\'t you join the dance. Will you, won\'t you, will you join the dance? \"You can really have no sort of lullaby to it as to prevent its undoing.','254','https://lorempixel.com/800/600/city/?18505','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(88,'Heidenreich PLC',2,NULL,'And she kept fanning herself all the jurymen are back in a sort of idea that they had settled down again very sadly and quietly, and looked at Alice. \'I\'M not a VERY unpleasant state of mind, she.','666','https://lorempixel.com/800/600/city/?98860','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(89,'Pfannerstill, Lowe and Moen',1,NULL,'Gryphon. \'How the creatures wouldn\'t be in a low voice, \'Your Majesty must cross-examine THIS witness.\' \'Well, if I chose,\' the Duchess and the happy summer days. THE.','708','https://lorempixel.com/800/600/city/?26841','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(90,'O\'Conner-Franecki',1,NULL,'Gryphon went on. Her listeners were perfectly quiet till she shook the house, \"Let us both go to law: I will prosecute YOU.--Come, I\'ll take no denial; We must have a trial: For really this morning.','101','https://lorempixel.com/800/600/city/?68128','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(91,'Abshire, O\'Hara and Dibbert',5,NULL,'Alice remained looking thoughtfully at the top of his shrill little voice, the name again!\' \'I won\'t interrupt again. I dare say you\'re wondering why I don\'t want to be?\' it asked. \'Oh, I\'m not used.','881','https://lorempixel.com/800/600/city/?72203','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(92,'Kulas PLC',8,NULL,'FIT you,\' said Alice, very loudly and decidedly, and there stood the Queen say only yesterday you deserved to be two people. \'But it\'s no use now,\' thought poor Alice, that she had quite forgotten.','154','https://lorempixel.com/800/600/city/?42323','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(93,'Fadel, Parisian and Watsica',8,NULL,'But said I didn\'t!\' interrupted Alice. \'You are,\' said the Mock Turtle\'s heavy sobs. Lastly, she pictured to herself what such an extraordinary ways of living would be grand, certainly,\' said Alice.','978','https://lorempixel.com/800/600/city/?61720','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(94,'Stehr Group',1,NULL,'Next came an angry voice--the Rabbit\'s--\'Pat! Pat! Where are you?\' And then a great crash, as if he doesn\'t begin.\' But she went on, \'What HAVE you been doing here?\' \'May it please your Majesty,\'.','974','https://lorempixel.com/800/600/city/?72946','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(95,'Lebsack-Bailey',7,NULL,'Dinah, if I chose,\' the Duchess said after a fashion, and this was not quite like the largest telescope that ever was! Good-bye, feet!\' (for when she found she could do to come out among the party.','48','https://lorempixel.com/800/600/city/?54037','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(96,'Deckow, Bogisich and Mohr',3,NULL,'Queen\'s shrill cries to the other, looking uneasily at the house, and wondering what to say to itself, half to itself, \'Oh dear! Oh dear! I\'d nearly forgotten to ask.\' \'It turned into a doze; but.','40','https://lorempixel.com/800/600/city/?74445','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(97,'Prohaska-Leffler',4,NULL,'King, \'that only makes the world you fly, Like a tea-tray in the distance. \'And yet what a wonderful dream it had no reason to be afraid of interrupting him,) \'I\'ll give him sixpence. _I_ don\'t.','434','https://lorempixel.com/800/600/city/?38697','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(98,'Durgan-Gleason',1,NULL,'King replied. Here the Dormouse shall!\' they both sat silent for a baby: altogether Alice did not wish to offend the Dormouse began in a day is very confusing.\' \'It isn\'t,\' said the Mock Turtle Soup.','455','https://lorempixel.com/800/600/city/?43300','1','2019-09-28 12:44:45','2019-09-28 12:44:45'),(99,'Dooley, Russel and Bergstrom',3,NULL,'Mock Turtle sang this, very slowly and sadly:-- \'\"Will you walk a little nervous about it while the Mouse replied rather crossly: \'of course you know that cats COULD grin.\' \'They all can,\' said the.','108','https://lorempixel.com/800/600/city/?83908','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(100,'Sauer, O\'Conner and Walter',5,NULL,'She was looking up into a graceful zigzag, and was delighted to find my way into that beautiful garden--how IS that to be two people. \'But it\'s no use in the direction it pointed to, without trying.','999','https://lorempixel.com/800/600/city/?69440','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(101,'Abshire-Schimmel',8,NULL,'Knave. The Knave shook his head contemptuously. \'I dare say you never tasted an egg!\' \'I HAVE tasted eggs, certainly,\' said Alice, rather alarmed at the mouth with strings: into this they slipped.','343','https://lorempixel.com/800/600/city/?28579','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(102,'Rogahn and Sons',3,NULL,'He got behind him, and very angrily. \'A knot!\' said Alice, timidly; \'some of the door that led into a butterfly, I should like to hear her try and repeat \"\'TIS THE VOICE OF THE SLUGGARD,\"\' said the.','407','https://lorempixel.com/800/600/city/?36091','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(103,'Dare, Leuschke and Pollich',6,NULL,'VERY nearly at the sudden change, but she did not feel encouraged to ask the question?\' said the youth, \'as I mentioned before, And have grown most uncommonly fat; Yet you finished the first verse,\'.','395','https://lorempixel.com/800/600/city/?83122','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(104,'Fisher-Reichert',4,NULL,'Cat. \'I said pig,\' replied Alice; \'and I wish I could let you out, you know.\' \'And what an ignorant little girl or a watch to take MORE than nothing.\' \'Nobody asked YOUR opinion,\' said Alice. \'It.','460','https://lorempixel.com/800/600/city/?43974','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(105,'Hodkiewicz and Sons',1,NULL,'I suppose it doesn\'t understand English,\' thought Alice; \'I might as well say,\' added the Gryphon, sighing in his note-book, cackled out \'Silence!\' and read as follows:-- \'The Queen of Hearts.','389','https://lorempixel.com/800/600/city/?63949','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(106,'Pacocha-Heidenreich',2,NULL,'Alice was so ordered about in the after-time, be herself a grown woman; and how she would gather about her pet: \'Dinah\'s our cat. And she\'s such a nice soft thing to get out again. The rabbit-hole.','402','https://lorempixel.com/800/600/city/?45273','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(107,'Dach LLC',2,NULL,'Mock Turtle is.\' \'It\'s the stupidest tea-party I ever heard!\' \'Yes, I think I must go and live in that case I can go back by railway,\' she said this, she looked back once or twice, half hoping that.','981','https://lorempixel.com/800/600/city/?36047','1','2019-09-28 12:44:46','2019-09-28 12:44:46'),(108,'Corwin-Ortiz',3,NULL,'It was so large in the air. \'--as far out to sea!\" But the snail replied \"Too far, too far!\" and gave a little pattering of footsteps in the last few minutes she heard something splashing about in a.','65','https://lorempixel.com/800/600/city/?46321','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(109,'Welch Inc',1,NULL,'Who for such dainties would not join the dance? Will you, won\'t you join the dance. So they got thrown out to be two people. \'But it\'s no use going back to them, and all her coaxing. Hardly knowing.','147','https://lorempixel.com/800/600/city/?38683','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(110,'Runolfsson-Koepp',3,NULL,'While she was peering about anxiously among the trees as well say,\' added the Gryphon; and then keep tight hold of it; and while she ran, as well as she could. \'The game\'s going on between the.','752','https://lorempixel.com/800/600/city/?43439','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(111,'O\'Hara, Hahn and Kuvalis',5,NULL,'March Hare said to herself; \'his eyes are so VERY remarkable in that; nor did Alice think it so quickly that the Queen said to herself, as usual. I wonder what they\'ll do next! As for pulling me out.','392','https://lorempixel.com/800/600/city/?18241','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(112,'Haag-Thiel',6,NULL,'Hatter: \'let\'s all move one place on.\' He moved on as he said to herself, as she could have been that,\' said the Duchess; \'and the moral of that is, but I can\'t quite follow it as far as they lay on.','337','https://lorempixel.com/800/600/city/?66285','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(113,'Rosenbaum Group',7,NULL,'Gryphon, and all of you, and must know better\'; and this Alice would not open any of them. \'I\'m sure those are not attending!\' said the Dormouse; \'VERY ill.\' Alice tried to say \"HOW DOTH THE LITTLE.','459','https://lorempixel.com/800/600/city/?93465','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(114,'Wiza, Champlin and VonRueden',5,NULL,'The first witness was the only difficulty was, that her flamingo was gone across to the table, but it all is! I\'ll try if I fell off the subjects on his spectacles. \'Where shall I begin, please your.','884','https://lorempixel.com/800/600/city/?60560','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(115,'Kuphal Ltd',6,NULL,'Duchess said after a few minutes, and she told her sister, who was trembling down to look at a king,\' said Alice. \'I\'ve so often read in the book,\' said the Caterpillar angrily, rearing itself.','515','https://lorempixel.com/800/600/city/?39428','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(116,'Kuphal, Sauer and Bruen',4,NULL,'This seemed to be an old crab, HE was.\' \'I never was so large a house, that she was quite a large fan in the same when I was a dispute going on between the executioner, the King, \'unless it was YOUR.','101','https://lorempixel.com/800/600/city/?24886','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(117,'Veum PLC',3,NULL,'Hatter, and, just as she spoke, but no result seemed to quiver all over crumbs.\' \'You\'re wrong about the right size for ten minutes together!\' \'Can\'t remember WHAT things?\' said the King. \'Shan\'t,\'.','389','https://lorempixel.com/800/600/city/?53236','1','2019-09-28 12:44:47','2019-09-28 12:44:47'),(118,'Ebert, Watsica and Bashirian',7,NULL,'Duchess and the Dormouse followed him: the March Hare moved into the sky. Alice went on again:-- \'I didn\'t know that Cheshire cats always grinned; in fact, I didn\'t know how to set them free.','35','https://lorempixel.com/800/600/city/?60425','1','2019-09-28 12:44:48','2019-09-28 12:44:48'),(119,'Little-Dare',8,NULL,'English, who wanted leaders, and had just begun to think that very few little girls eat eggs quite as safe to stay in here any longer!\' She waited for a minute, trying to fix on one, the cook was.','265','https://lorempixel.com/800/600/city/?45018','1','2019-09-28 12:44:48','2019-09-28 12:44:48'),(120,'Romaguera, Emard and Howe',6,NULL,'Alice could only see her. She is such a capital one for catching mice you can\'t take LESS,\' said the Caterpillar angrily, rearing itself upright as it was a queer-shaped little creature, and held it.','391','https://lorempixel.com/800/600/city/?43830','1','2019-09-28 12:44:48','2019-09-28 12:44:48');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_09_28_021024_create_categories_table',1),(4,'2019_09_28_021101_create_sub_categories_table',1),(5,'2019_09_28_021135_create_items_table',1),(6,'2019_09_28_021309_create_purchase_orders_table',1),(7,'2019_09_28_021326_create_carts_table',1),(8,'2019_09_28_021547_create_transactions_table',1),(9,'2019_09_28_021842_create_suppliers_table',1),(10,'2019_09_28_021853_create_coupons_table',1),(11,'2019_09_28_021932_create_deductions_table',1),(12,'2019_09_28_021950_create_company_approvers_table',1),(13,'2019_09_28_053708_create_quotations_table',1),(14,'2019_09_28_075722_create_biddings_table',1),(15,'2019_09_28_092237_edit_user',1),(16,'2019_09_28_095737_product',1),(17,'2019_09_28_195731_edit_biddings',2),(19,'2019_09_28_200212_edit_products',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SKU` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `units_sold` decimal(8,2) DEFAULT NULL,
  `estimated_time_days` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Tisyu 3','000079',3,1,1,'Incidunt sed a consequatur vero praesentium. Quia a quidem necessitatibus accusantium. Velit saepe tempore a velit quisquam.',NULL,'2019-09-28 11:34:58','2019-09-28 13:34:05',NULL,10,736),(2,'Tisyu 3','000085',2,1,1,'Nobis sed deserunt dolorum ipsum illum et voluptas. Voluptatem reprehenderit eos unde iste saepe. Sed earum in consequatur expedita est rerum est.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,386),(3,'Tisyu 1','000043',5,1,1,'Et et omnis asperiores rerum aperiam dolor. Nulla enim aut optio fuga alias est. Deserunt sint tenetur enim quia fugiat aut.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,873),(4,'Tisyu 4','000093',1,1,1,'Eum et nulla quidem amet ratione voluptatem. Tenetur rerum vero molestiae similique voluptatem. Qui laudantium et aut atque. Cupiditate id accusantium necessitatibus voluptas qui quam quo corrupti.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,777),(5,'Tisyu 3','tas000050',3,1,1,'Sit fugit assumenda tenetur corporis adipisci aliquam omnis. Fugiat neque rem cum beatae velit. Ipsa necessitatibus odit tempore ut.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,329),(6,'Tisyu 4','000016',3,1,1,'Accusantium enim provident numquam. Autem enim aspernatur enim commodi quia sequi. Temporibus autem et commodi vitae aspernatur dolor.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,501),(7,'Tisyu 3','tas000011',5,1,1,'Vitae officia voluptas amet repellendus ipsum est modi dolor. Id sed tempore enim possimus aperiam alias explicabo.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,822),(8,'Tisyu 4','000029',5,1,1,'Quas a dignissimos libero sed. Saepe ipsam similique sit deserunt qui dolores. Unde quo doloribus ratione ducimus aut esse. Autem et et non perferendis eius voluptate.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,346),(9,'Tisyu 1','000057',5,1,1,'Incidunt consequatur vel et eveniet quis sequi. Id ad et animi voluptas officiis. Consequatur rerum dicta suscipit.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,755),(10,'Tisyu 5','000078',3,1,1,'Id dolore fugiat architecto voluptas et voluptas. At repellat impedit accusamus exercitationem sed quibusdam et. Quidem quam quis id odit voluptate.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,26),(11,'Tisyu 1','000067',2,1,1,'Omnis est similique maxime omnis modi hic sint. Vero qui quo cupiditate accusamus repellendus ad. Exercitationem est deleniti doloribus tempore nisi molestias consequatur.',NULL,'2019-09-28 11:34:59','2019-09-28 13:34:05',NULL,NULL,914);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_orders`
--

DROP TABLE IF EXISTS `purchase_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_orders`
--

LOCK TABLES `purchase_orders` WRITE;
/*!40000 ALTER TABLE `purchase_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotations`
--

DROP TABLE IF EXISTS `quotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quotation_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_quotation_num` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `start_timer` datetime DEFAULT NULL,
  `end_timer` datetime DEFAULT NULL,
  `years_joined` int(11) DEFAULT NULL,
  `item_sales` decimal(11,2) DEFAULT NULL,
  `warranty_years` int(11) DEFAULT NULL,
  `estimated_time_days` int(11) DEFAULT NULL,
  `cost_per_piece` decimal(11,2) DEFAULT NULL,
  `total_cost` decimal(11,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotations`
--

LOCK TABLES `quotations` WRITE;
/*!40000 ALTER TABLE `quotations` DISABLE KEYS */;
INSERT INTO `quotations` VALUES (1,'1','2019000001',3,11,2,1,2000,NULL,NULL,NULL,NULL,1,8,59.00,118000.00,'2019-09-28 14:10:40','2019-09-28 13:18:23'),(2,'2','2019000002',3,3,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-28 14:13:26','2019-09-28 13:18:24'),(3,'3','2019000003',5,3,2,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-28 10:42:33','2019-09-28 13:18:24');
/*!40000 ALTER TABLE `quotations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_categories`
--

LOCK TABLES `sub_categories` WRITE;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Nicolas-McDermott','98924 Randall Underpass\nPfefferbury, DE 36731-5627','verified','2019-09-28 10:56:16','2019-09-28 10:56:16'),(2,'Leannon-Veum','13021 Rickie Cliff Suite 494\nEast Michealberg, ME 71414-7409','verified','2019-09-28 10:56:16','2019-09-28 10:56:16'),(3,'Strosin LLC','558 Lowe Canyon\nLake Reidland, ME 23631-3723','verified','2019-09-28 10:56:16','2019-09-28 10:56:16'),(4,'Marquardt-Schamberger','5195 Rau Rapid\nNayelishire, TX 65964','verified','2019-09-28 10:56:16','2019-09-28 10:56:16'),(5,'O\'Reilly-Haag','667 O\'Keefe Viaduct\nEast Samarahaven, TN 58298-5590','verified','2019-09-28 10:56:16','2019-09-28 10:56:16'),(6,'Weber-Olson','47584 Boyd Prairie Apt. 924\nLake Jonathan, NC 10478','verified','2019-09-28 10:56:16','2019-09-28 10:56:16');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `or_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `qty` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transactions_or_number_unique` (`or_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','Webmaster','Super Admin','admin@test.com',NULL,'$2y$10$x8WNYna2uOMPEPq8y8Fuuu7TTeX2jRB8PmnexOody3gd9esx3C9Ui','1',NULL,'2019-09-28 12:44:28','2019-09-28 12:44:28',NULL),(2,'Ryleigh','Bogisich','Mine Cutting Machine Operator','merle.emard@ratke.info','1985-09-13 17:18:04','$2y$10$yPfrbP.gjin2kVXOHFbTn.bzjr6Wq.zkZ.EE4GFDbrIzcrTmo4yPe','verified',NULL,'2019-09-28 10:41:19','2019-09-28 10:58:20',1),(3,'Eric','Kirlin','Political Science Teacher','sbalistreri@kirlin.org','2003-09-04 02:26:16','$2y$10$Wg00oaUyKjn5RUCgI0k9x.EppqhW.QQmdGVwcoBll9Srb7e5IJmqK','verified',NULL,'2019-09-28 10:41:20','2019-09-28 10:41:20',1),(4,'Lilian','Bayer','Healthcare','timmothy.ernser@yahoo.com','1973-08-30 09:53:57','$2y$10$6IJcNmPTQXpGmOuRh/2v1eReYZtUVe3SDdhx372bKoYwjOJNKQnoi','verified',NULL,'2019-09-28 10:41:20','2019-09-28 10:41:20',2),(5,'Meredith','Dicki','Gas Compressor Operator','nigel59@hotmail.com','2016-09-24 06:34:06','$2y$10$S3Bcno313rBJax.6gshSo..np/eCuRSpsQND01n6oV1g3OzDgUCxC','verified',NULL,'2019-09-28 10:41:20','2019-09-28 10:41:20',3),(6,'Antonette','Kovacek','Marriage and Family Therapist','autumn.gutkowski@little.com','2015-05-14 18:04:03','$2y$10$Ng2r8bF.P7dA3CXe/Db1reMaPPZhtjte8MuM5HZxn8YLpVCstAd8.','verified',NULL,'2019-09-28 10:41:20','2019-09-28 10:41:20',4),(7,'Chyna','Sporer','Microbiologist','albertha.conn@smith.com','2007-11-12 09:42:09','$2y$10$apI0be1ioe4.f97GYX0F7.G/BUY1j7Ha24l5Z.DAXAGy6NIC633re','verified',NULL,'2019-09-28 10:41:20','2019-09-28 10:41:20',5);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-29  5:55:02
