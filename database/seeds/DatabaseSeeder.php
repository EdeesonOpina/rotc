<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'firstname'=>'Admin',
            'lastname'=>'Webmaster',
            'email'=>'admin@test.com',
            'password'=>bcrypt('123123123'),
            'role'=>'Super Admin',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        // CATEGORIES BY SICKNESS
        DB::table('categories')->insert([
            'description'=>'Carpet',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('categories')->insert([
            'description'=>'Tiles',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('categories')->insert([
            'description'=>'Office Supplies',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('categories')->insert([
            'description'=>'Tools',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('categories')->insert([
            'description'=>'Lighting',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

         DB::table('categories')->insert([
            'description'=>'Toiletries',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('categories')->insert([
            'description'=>'Plumbing',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('categories')->insert([
            'description'=>'Car Parts',
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        $items = factory(App\Item::class, 120)->create();


    }
}
