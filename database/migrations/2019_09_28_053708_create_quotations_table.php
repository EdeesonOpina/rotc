<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quotation_num');
            $table->integer('full_quotation_num');//
            $table->integer('category_id');
            $table->integer('item_id');
            $table->integer('user_id');
            $table->integer('company_id');//
            $table->integer('qty');
            $table->dateTime('start_timer')->nullable();
            $table->dateTime('end_timer')->nullable();

            $table->integer('years_joined')->nullable();
            $table->decimal('item_sales', 11, 2)->nullable();
            $table->integer('warranty_years')->nullable();
            $table->integer('estimated_time_days')->nullable();
            $table->decimal('cost_per_piece', 11, 2)->nullable();
            $table->decimal('total_cost', 11, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
