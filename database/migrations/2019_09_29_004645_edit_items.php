<?php

use App\Item;
use Faker\Factory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->decimal('units_sold')->nullable();
            $table->integer('estimated_time_days')->nullable();
            $table->string('SKU')->nullable();
            $table->integer('supplier_id')->nullable();
        });

        $faker = Factory::create();

        $items = Item::all();

        foreach ($items as $i) {
            $i->SKU = $faker->word . '000' . rand(11,99);
            $i->supplier_id = rand(1,5);
            $i->units_sold = rand(100, 20000);
            $i->estimated_time_days = rand(8,30);
            $i->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
